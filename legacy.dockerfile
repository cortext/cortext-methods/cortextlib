# docker build . -t cortext-methods/legacy

FROM debian:9

LABEL maintainer="Ale Abdo <abdo@member.fsf.org>"

# apt packages
RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y curl
RUN apt-get install -y unzip
RUN apt-get install -y wget
RUN apt-get install -y nano
#RUN apt-get install -y php5
RUN apt-get install -y ghostscript
RUN apt-get install -y enscript
RUN apt-get install -y r-base r-base-dev
RUN apt-get install -y libgeos-dev
RUN apt-get install -y libgdal-dev

# python and pip packages

RUN apt-get install -y python
RUN apt-get install -y python-pip
RUN pip install setuptools
RUN pip install wheel

#### On host machine: ####
## Requires 'pipreqs' with my patch: https://github.com/bndr/pipreqs/pull/250
####
## #! /usr/bin/env sh
## CORTEXT_DIR=/srv/lisis-prod/web/cortext/cortext-methods
## find $CORTEXT_DIR -maxdepth 1 -type d -exec ~/.local/bin/pipreqs --use-local \
##   --print {} 2>/dev/null \; | sort -u > cortext_legacy_requirements.txt
####
COPY cortext_legacy_requirements.txt ./
RUN pip install -r ./cortext_legacy_requirements.txt
RUN pip install PyYAML==3.11
RUN pip install statistics==1.0.3.5
#### / ####

RUN python -m spacy download it
RUN python -m spacy download fr
RUN python -m spacy download en
RUN python -m spacy download de
RUN python -m spacy download es
RUN python -m spacy download xx
RUN python -c 'import nltk; nltk.download("punkt")'

# relevant paths
ARG BASE_PATH=/srv
ARG SETUP_PATH=$BASE_PATH/setup
ARG APP_PATH=$BASE_PATH/cortext
ARG METHODS_PATH=$BASE_PATH/cortext-methods

## WORKDIR $CONFIG_FILES_PATH/cortext/
## 
## RUN ./install-scripts.sh
## 
## # Download and put in place some things that are not mentioned anywhere
## WORKDIR $APP_PATH/cortext-methods
## 
## ## distant reading script won't work if methods are not on /srv/local/web/cortext/cortext-methods
## RUN mkdir -p /srv/local/web/cortext/manager
## RUN ln -s $APP_PATH/cortext-methods /srv/local/web/cortext/manager/scripts
## 
## ## "lib" folder
## RUN wget -c --show-progress http://file.inra-ifris.org/files/lib.tar.gz
## RUN tar xvf lib.tar.gz
## RUN rm lib.tar.gz
## 
## ### There's a symbolic link to it in assets/server/documents
## RUN ln -s $APP_PATH/cortext-methods/lib/ $APP_PATH/cortext-assets/server/documents/lib
## 
## ## terms_extractor/ngrams.db
## RUN wget -c --show-progress http://file.inra-ifris.org/files/ngrams.db -O $APP_PATH/cortext-methods/terms_extractor/ngrams.db
