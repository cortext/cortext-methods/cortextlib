# An example method for use with Cortextlib

This example is to be used as a reference to improve existing methods and to develop new ones.

## Creating a new method
Copy over the contents of this directory and rename everything from "example method" to your method's name.
