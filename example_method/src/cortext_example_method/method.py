from cortextlib import CortextMethod

# Import the module 'example_analysis.py' found in the same directory.
from . import example_analysis


class Method(CortextMethod):
    """
    Example class implementing the essential pattern for a method.

    Tips:
    - self.main(): the class method that gets called after instantiation.
    - self.params: the jobs's user provided parameters
    - self.data: the job's database <-> pandas.DataFrame interface
    """

    def __init__(self, job):
        """
        Initialize base class and process parameters (i.e. `self.params`) so its items
        fit the signature of method `main()`.
        """
        super().__init__(job)
        # In this example we want to pop parameter 'field0_type' and, in case
        # its value is "multi", split parameter 'field0' at whitespaces
        if self.params.pop("field0_type") == "multi":
            self.params["field0"] = self.params["field0"].split()

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Exit base class and run cleanup logic, if any.
        """
        super().__exit__(exc_type, exc_value, traceback)

    def main(self, field0, field1, model_parameter0):
        """
        This method gets called with the items of `self.params` as arguments.

        It should contain the major steps of the actual analysis.  If
        appropriate, call other instance methods to reflect major alternatives.
        *In any case, do not implement the details of the analysis in this
        class.* Implement them in other files in the same directory or an
        external library.

        If you need to use an attribute or method of `self.job`, do not pass the
        `Job` instance to functions, but only that attribute or method. This is so
        we can see from here what is being used where.
        """
        # Locally shorten some names for readability
        logger = self.job.logger

        logger.info("Get only the data we need")
        data, data_schema = self.data.to_dataframe(columns=[field0, field1])

        logger.info("Process data")
        processed_data = example_analysis.process_data(self.data, logger)

        logger.info("Fit statistical model")
        stats, new_data = example_analysis.heavy_statistics(
            processed_data, model_parameter0, logger
        )

        logger.info("Create visualization")
        fname, fcontent = example_analysis.create_visualisation(
            processed_data, stats, logger
        )

        logger.info("Maybe we want to store part of our modifications to existing data")
        self.data.from_dataframe(processed_data.loc[:, [field1]], data_schema)

        logger.info("Storing new data, let's say it's a rank level list")
        self.data.from_dataframe(new_data, data_schema={"new_column": ["rank"]})

        logger.info("Storing visualisation in job result path")
        (self.job.dir / fname).write_bytes(fcontent)
