from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import Union
from ...types import UNSET, Unset
from ...models.error import Error
from typing import cast
from typing import Dict
from ...models.get_documents_response_200 import GetDocumentsResponse200


def _get_kwargs(
    *,
    client: Client,
    user: str,
    allowed_extensions: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    url = "{}/documents/".format(client.base_url)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    params: Dict[str, Any] = {
        "user": user,
        "allowedExtensions": allowed_extensions,
    }
    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "params": params,
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, GetDocumentsResponse200]]:
    if response.status_code == 200:
        response_200 = GetDocumentsResponse200.from_dict(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, GetDocumentsResponse200]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    user: str,
    allowed_extensions: Union[Unset, str] = UNSET,
) -> Response[Union[Error, GetDocumentsResponse200]]:
    kwargs = _get_kwargs(
        client=client,
        user=user,
        allowed_extensions=allowed_extensions,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    user: str,
    allowed_extensions: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, GetDocumentsResponse200]]:
    """ Retrieve metadata of documents of current user """

    return sync_detailed(
        client=client,
        user=user,
        allowed_extensions=allowed_extensions,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    user: str,
    allowed_extensions: Union[Unset, str] = UNSET,
) -> Response[Union[Error, GetDocumentsResponse200]]:
    kwargs = _get_kwargs(
        client=client,
        user=user,
        allowed_extensions=allowed_extensions,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    user: str,
    allowed_extensions: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, GetDocumentsResponse200]]:
    """ Retrieve metadata of documents of current user """

    return (
        await asyncio_detailed(
            client=client,
            user=user,
            allowed_extensions=allowed_extensions,
        )
    ).parsed
