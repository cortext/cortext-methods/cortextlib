from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import cast
from ...models.error import Error
from typing import Dict
from ...models.document import Document
from ...models.patch_documents_document_id_json_body import PatchDocumentsDocumentIdJsonBody


def _get_kwargs(
    *,
    client: Client,
    document_id: str,
    json_body: PatchDocumentsDocumentIdJsonBody,
) -> Dict[str, Any]:
    url = "{}/documents/{documentId}".format(client.base_url, documentId=document_id)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    json_json_body = json_body.to_dict()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "json": json_json_body,
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Document, Error]]:
    if response.status_code == 200:
        response_200 = Document.from_dict(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Document, Error]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    document_id: str,
    json_body: PatchDocumentsDocumentIdJsonBody,
) -> Response[Union[Document, Error]]:
    kwargs = _get_kwargs(
        client=client,
        document_id=document_id,
        json_body=json_body,
    )

    response = httpx.patch(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    document_id: str,
    json_body: PatchDocumentsDocumentIdJsonBody,
) -> Optional[Union[Document, Error]]:
    """  """

    return sync_detailed(
        client=client,
        document_id=document_id,
        json_body=json_body,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    document_id: str,
    json_body: PatchDocumentsDocumentIdJsonBody,
) -> Response[Union[Document, Error]]:
    kwargs = _get_kwargs(
        client=client,
        document_id=document_id,
        json_body=json_body,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.patch(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    document_id: str,
    json_body: PatchDocumentsDocumentIdJsonBody,
) -> Optional[Union[Document, Error]]:
    """  """

    return (
        await asyncio_detailed(
            client=client,
            document_id=document_id,
            json_body=json_body,
        )
    ).parsed
