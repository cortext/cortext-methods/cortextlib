from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import cast
from ...models.error import Error
from ...models.project import Project
from typing import Dict


def _get_kwargs(
    *,
    client: Client,
    project_id: str,
    analysis_id: str,
) -> Dict[str, Any]:
    url = "{}/project/{projectId}/analysis/{analysisId}".format(client.base_url, projectId=project_id, analysisId=analysis_id)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, Project]]:
    if response.status_code == 200:
        response_200 = Project.from_dict(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, Project]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    project_id: str,
    analysis_id: str,
) -> Response[Union[Error, Project]]:
    kwargs = _get_kwargs(
        client=client,
        project_id=project_id,
        analysis_id=analysis_id,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    project_id: str,
    analysis_id: str,
) -> Optional[Union[Error, Project]]:
    """ Metadata of a project """

    return sync_detailed(
        client=client,
        project_id=project_id,
        analysis_id=analysis_id,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    project_id: str,
    analysis_id: str,
) -> Response[Union[Error, Project]]:
    kwargs = _get_kwargs(
        client=client,
        project_id=project_id,
        analysis_id=analysis_id,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    project_id: str,
    analysis_id: str,
) -> Optional[Union[Error, Project]]:
    """ Metadata of a project """

    return (
        await asyncio_detailed(
            client=client,
            project_id=project_id,
            analysis_id=analysis_id,
        )
    ).parsed
