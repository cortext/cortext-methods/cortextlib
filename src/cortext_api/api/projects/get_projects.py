from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import Union
from ...types import UNSET, Unset
from ...models.error import Error
from typing import cast
from typing import Dict
from typing import cast, List
from ...models.project import Project


def _get_kwargs(
    *,
    client: Client,
    user: Union[Unset, int] = UNSET,
) -> Dict[str, Any]:
    url = "{}/projects/".format(client.base_url)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    params: Dict[str, Any] = {
        "user": user,
    }
    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "params": params,
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, List[Project]]]:
    if response.status_code == 200:
        response_200 = []
        _response_200 = response.json()
        for response_200_item_data in _response_200:
            response_200_item = Project.from_dict(response_200_item_data)

            response_200.append(response_200_item)

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, List[Project]]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    user: Union[Unset, int] = UNSET,
) -> Response[Union[Error, List[Project]]]:
    kwargs = _get_kwargs(
        client=client,
        user=user,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    user: Union[Unset, int] = UNSET,
) -> Optional[Union[Error, List[Project]]]:
    """ List of available projects with basic filter """

    return sync_detailed(
        client=client,
        user=user,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    user: Union[Unset, int] = UNSET,
) -> Response[Union[Error, List[Project]]]:
    kwargs = _get_kwargs(
        client=client,
        user=user,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    user: Union[Unset, int] = UNSET,
) -> Optional[Union[Error, List[Project]]]:
    """ List of available projects with basic filter """

    return (
        await asyncio_detailed(
            client=client,
            user=user,
        )
    ).parsed
