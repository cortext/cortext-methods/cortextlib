from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import cast
from ...models.error import Error
from ...models.get_scripts_script_id_response_200 import GetScriptsScriptIdResponse200
from typing import Dict


def _get_kwargs(
    *,
    client: Client,
    script_id: str,
) -> Dict[str, Any]:
    url = "{}/scripts/{scriptId}".format(client.base_url, scriptId=script_id)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, GetScriptsScriptIdResponse200]]:
    if response.status_code == 200:
        response_200 = GetScriptsScriptIdResponse200.from_dict(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, GetScriptsScriptIdResponse200]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    script_id: str,
) -> Response[Union[Error, GetScriptsScriptIdResponse200]]:
    kwargs = _get_kwargs(
        client=client,
        script_id=script_id,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    script_id: str,
) -> Optional[Union[Error, GetScriptsScriptIdResponse200]]:
    """ Retrieve parameters list of a specific script """

    return sync_detailed(
        client=client,
        script_id=script_id,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    script_id: str,
) -> Response[Union[Error, GetScriptsScriptIdResponse200]]:
    kwargs = _get_kwargs(
        client=client,
        script_id=script_id,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    script_id: str,
) -> Optional[Union[Error, GetScriptsScriptIdResponse200]]:
    """ Retrieve parameters list of a specific script """

    return (
        await asyncio_detailed(
            client=client,
            script_id=script_id,
        )
    ).parsed
