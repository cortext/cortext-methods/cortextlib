from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from ...models.stats_item import StatsItem
from typing import cast
from ...models.error import Error
from ...models.status import Status
from typing import Dict
from typing import cast, List
from typing import cast, Union


def _get_kwargs(
    *,
    client: Client,
) -> Dict[str, Any]:
    url = "{}/stats/".format(client.base_url)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, Union[List[StatsItem], Status]]]:
    if response.status_code == 200:

        def _parse_response_200(data: object) -> Union[List[StatsItem], Status]:
            try:
                if not isinstance(data, list):
                    raise TypeError()
                response_200_type_0 = UNSET
                _response_200_type_0 = data
                for componentsschemasstats_item_data in _response_200_type_0:
                    componentsschemasstats_item = StatsItem.from_dict(componentsschemasstats_item_data)

                    response_200_type_0.append(componentsschemasstats_item)

                return response_200_type_0
            except:  # noqa: E722
                pass
            if not isinstance(data, dict):
                raise TypeError()
            response_200_type_1 = Status.from_dict(data)

            return response_200_type_1

        response_200 = _parse_response_200(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, Union[List[StatsItem], Status]]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
) -> Response[Union[Error, Union[List[StatsItem], Status]]]:
    kwargs = _get_kwargs(
        client=client,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
) -> Optional[Union[Error, Union[List[StatsItem], Status]]]:
    """ Statistics of cortext manager uses """

    return sync_detailed(
        client=client,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
) -> Response[Union[Error, Union[List[StatsItem], Status]]]:
    kwargs = _get_kwargs(
        client=client,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
) -> Optional[Union[Error, Union[List[StatsItem], Status]]]:
    """ Statistics of cortext manager uses """

    return (
        await asyncio_detailed(
            client=client,
        )
    ).parsed
