from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from ...models.stats_item import StatsItem
from typing import cast
from ...models.error import Error
from ...models.get_stats_label_label import GetStatsLabelLabel
from typing import Dict
from typing import cast, List


def _get_kwargs(
    *,
    client: Client,
    label: GetStatsLabelLabel,
) -> Dict[str, Any]:
    url = "{}/stats/{label}".format(client.base_url, label=label)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, List[StatsItem]]]:
    if response.status_code == 200:
        response_200 = []
        _response_200 = response.json()
        for componentsschemasstats_item_data in _response_200:
            componentsschemasstats_item = StatsItem.from_dict(componentsschemasstats_item_data)

            response_200.append(componentsschemasstats_item)

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, List[StatsItem]]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    label: GetStatsLabelLabel,
) -> Response[Union[Error, List[StatsItem]]]:
    kwargs = _get_kwargs(
        client=client,
        label=label,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    label: GetStatsLabelLabel,
) -> Optional[Union[Error, List[StatsItem]]]:
    """ Specific statistics of cortext manager uses """

    return sync_detailed(
        client=client,
        label=label,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    label: GetStatsLabelLabel,
) -> Response[Union[Error, List[StatsItem]]]:
    kwargs = _get_kwargs(
        client=client,
        label=label,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    label: GetStatsLabelLabel,
) -> Optional[Union[Error, List[StatsItem]]]:
    """ Specific statistics of cortext manager uses """

    return (
        await asyncio_detailed(
            client=client,
            label=label,
        )
    ).parsed
