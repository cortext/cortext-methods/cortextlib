from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import cast
from ...models.error import Error
from typing import Dict
from ...models.job import Job


def _get_kwargs(
    *,
    client: Client,
    job_id: int,
) -> Dict[str, Any]:
    url = "{}/jobs/{jobId}".format(client.base_url, jobId=job_id)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, Job]]:
    if response.status_code == 200:
        response_200 = Job.from_dict(response.json())

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, Job]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    job_id: int,
) -> Response[Union[Error, Job]]:
    kwargs = _get_kwargs(
        client=client,
        job_id=job_id,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    job_id: int,
) -> Optional[Union[Error, Job]]:
    """ Metadata of a specific job """

    return sync_detailed(
        client=client,
        job_id=job_id,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    job_id: int,
) -> Response[Union[Error, Job]]:
    kwargs = _get_kwargs(
        client=client,
        job_id=job_id,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    job_id: int,
) -> Optional[Union[Error, Job]]:
    """ Metadata of a specific job """

    return (
        await asyncio_detailed(
            client=client,
            job_id=job_id,
        )
    ).parsed
