from typing import Any, Dict, List, Optional, Union, cast

import httpx

from ...client import AuthenticatedClient, Client
from ...types import Response, UNSET

from typing import Union
from ...types import UNSET, Unset
from ...models.error import Error
from typing import cast
from typing import Dict
from ...models.job import Job
from typing import cast, List


def _get_kwargs(
    *,
    client: Client,
    project: Union[Unset, int] = UNSET,
    script_id: Union[Unset, int] = UNSET,
    user: Union[Unset, int] = UNSET,
    state: Union[Unset, int] = UNSET,
    label: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    url = "{}/jobs/".format(client.base_url)

    headers: Dict[str, Any] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    params: Dict[str, Any] = {
        "project": project,
        "script_id": script_id,
        "user": user,
        "state": state,
        "label": label,
    }
    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    return {
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "params": params,
    }


def _parse_response(*, response: httpx.Response) -> Optional[Union[Error, List[Job]]]:
    if response.status_code == 200:
        response_200 = []
        _response_200 = response.json()
        for response_200_item_data in _response_200:
            response_200_item = Job.from_dict(response_200_item_data)

            response_200.append(response_200_item)

        return response_200
    if response.status_code == 400:
        response_400 = Error.from_dict(response.json())

        return response_400
    return None


def _build_response(*, response: httpx.Response) -> Response[Union[Error, List[Job]]]:
    return Response(
        status_code=response.status_code,
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(response=response),
    )


def sync_detailed(
    *,
    client: Client,
    project: Union[Unset, int] = UNSET,
    script_id: Union[Unset, int] = UNSET,
    user: Union[Unset, int] = UNSET,
    state: Union[Unset, int] = UNSET,
    label: Union[Unset, str] = UNSET,
) -> Response[Union[Error, List[Job]]]:
    kwargs = _get_kwargs(
        client=client,
        project=project,
        script_id=script_id,
        user=user,
        state=state,
        label=label,
    )

    response = httpx.get(
        **kwargs,
    )

    return _build_response(response=response)


def sync(
    *,
    client: Client,
    project: Union[Unset, int] = UNSET,
    script_id: Union[Unset, int] = UNSET,
    user: Union[Unset, int] = UNSET,
    state: Union[Unset, int] = UNSET,
    label: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, List[Job]]]:
    """ List all jobs with basic filter """

    return sync_detailed(
        client=client,
        project=project,
        script_id=script_id,
        user=user,
        state=state,
        label=label,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    project: Union[Unset, int] = UNSET,
    script_id: Union[Unset, int] = UNSET,
    user: Union[Unset, int] = UNSET,
    state: Union[Unset, int] = UNSET,
    label: Union[Unset, str] = UNSET,
) -> Response[Union[Error, List[Job]]]:
    kwargs = _get_kwargs(
        client=client,
        project=project,
        script_id=script_id,
        user=user,
        state=state,
        label=label,
    )

    async with httpx.AsyncClient() as _client:
        response = await _client.get(**kwargs)

    return _build_response(response=response)


async def asyncio(
    *,
    client: Client,
    project: Union[Unset, int] = UNSET,
    script_id: Union[Unset, int] = UNSET,
    user: Union[Unset, int] = UNSET,
    state: Union[Unset, int] = UNSET,
    label: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, List[Job]]]:
    """ List all jobs with basic filter """

    return (
        await asyncio_detailed(
            client=client,
            project=project,
            script_id=script_id,
            user=user,
            state=state,
            label=label,
        )
    ).parsed
