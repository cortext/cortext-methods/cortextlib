from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr
import json

from ..types import UNSET, Unset

from io import BytesIO
from typing import Union
from ..types import UNSET, Unset
from ..types import File, FileJsonType


T = TypeVar("T", bound="PostFilesMultipartData")


@attr.s(auto_attribs=True)
class PostFilesMultipartData:
    """  """

    qqfile: Union[Unset, File] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        qqfile: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.qqfile, Unset):
            qqfile = self.qqfile.to_tuple()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if qqfile is not UNSET:
            field_dict["qqfile"] = qqfile

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        qqfile: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.qqfile, Unset):
            qqfile = self.qqfile.to_tuple()

        field_dict: Dict[str, Any] = {}
        field_dict.update({key: (None, str(value), "text/plain") for key, value in self.additional_properties.items()})
        field_dict.update({})
        if qqfile is not UNSET:
            field_dict["qqfile"] = qqfile

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _qqfile = d.pop("qqfile", UNSET)
        qqfile: Union[Unset, File]
        if isinstance(_qqfile, Unset):
            qqfile = UNSET
        else:
            qqfile = File(payload=BytesIO(_qqfile))

        post_files_multipart_data = cls(
            qqfile=qqfile,
        )

        post_files_multipart_data.additional_properties = d
        return post_files_multipart_data

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
