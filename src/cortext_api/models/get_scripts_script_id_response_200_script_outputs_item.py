from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from ..types import UNSET, Unset


T = TypeVar("T", bound="GetScriptsScriptIdResponse200ScriptOutputsItem")


@attr.s(auto_attribs=True)
class GetScriptsScriptIdResponse200ScriptOutputsItem:
    """  """

    type: Union[Unset, str] = UNSET
    categ: Union[Unset, str] = UNSET
    structure: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        type = self.type
        categ = self.categ
        structure = self.structure

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if type is not UNSET:
            field_dict["type"] = type
        if categ is not UNSET:
            field_dict["categ"] = categ
        if structure is not UNSET:
            field_dict["structure"] = structure

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        type = d.pop("type", UNSET)

        categ = d.pop("categ", UNSET)

        structure = d.pop("structure", UNSET)

        get_scripts_script_id_response_200_script_outputs_item = cls(
            type=type,
            categ=categ,
            structure=structure,
        )

        get_scripts_script_id_response_200_script_outputs_item.additional_properties = d
        return get_scripts_script_id_response_200_script_outputs_item

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
