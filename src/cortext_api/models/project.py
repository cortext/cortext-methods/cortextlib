from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.user import User
from typing import Dict
from typing import cast, List
from ..models.project_composition_item import ProjectCompositionItem




T = TypeVar("T", bound="Project")

@attr.s(auto_attribs=True)
class Project:
    """  """
    id: Union[Unset, int] = UNSET
    title: Union[Unset, str] = UNSET
    date_created: Union[Unset, str] = UNSET
    date_updated: Union[Unset, str] = UNSET
    members: Union[Unset, List[User]] = UNSET
    permalink: Union[Unset, str] = UNSET
    composition: Union[Unset, List[ProjectCompositionItem]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)


    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        title = self.title
        date_created = self.date_created
        date_updated = self.date_updated
        members: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.members, Unset):
            members = []
            for members_item_data in self.members:
                members_item = members_item_data.to_dict()

                members.append(members_item)




        permalink = self.permalink
        composition: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.composition, Unset):
            composition = []
            for composition_item_data in self.composition:
                composition_item = composition_item_data.to_dict()

                composition.append(composition_item)





        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({
        })
        if id is not UNSET:
            field_dict["id"] = id
        if title is not UNSET:
            field_dict["title"] = title
        if date_created is not UNSET:
            field_dict["date_created"] = date_created
        if date_updated is not UNSET:
            field_dict["date_updated"] = date_updated
        if members is not UNSET:
            field_dict["members"] = members
        if permalink is not UNSET:
            field_dict["permalink"] = permalink
        if composition is not UNSET:
            field_dict["composition"] = composition

        return field_dict



    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        if isinstance(src_dict, list):  # workaround for https://gitlab.com/cortext/cortext-monitoring/-/issues/18
            src_dict, = src_dict
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        title = d.pop("title", UNSET)

        date_created = d.pop("date_created", UNSET)

        date_updated = d.pop("date_updated", UNSET)

        members = []
        _members = d.pop("members", UNSET)
        for members_item_data in (_members or []):
            members_item = User.from_dict(members_item_data)



            members.append(members_item)


        permalink = d.pop("permalink", UNSET)

        composition = []
        _composition = d.pop("composition", UNSET)
        for composition_item_data in (_composition or []):
            composition_item = ProjectCompositionItem.from_dict(composition_item_data)



            composition.append(composition_item)


        project = cls(
            id=id,
            title=title,
            date_created=date_created,
            date_updated=date_updated,
            members=members,
            permalink=permalink,
            composition=composition,
        )

        project.additional_properties = d
        return project

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
