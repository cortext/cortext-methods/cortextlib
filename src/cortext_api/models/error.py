from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from typing import Dict
from typing import cast, List
from ..models.error_exception_item import ErrorExceptionItem


T = TypeVar("T", bound="Error")


@attr.s(auto_attribs=True)
class Error:
    """  """

    code: int
    message: str
    exception: Union[Unset, List[ErrorExceptionItem]] = UNSET
    fields: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        code = self.code
        message = self.message
        exception: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.exception, Unset):
            exception = []
            for exception_item_data in self.exception:
                exception_item = exception_item_data.to_dict()

                exception.append(exception_item)

        fields = self.fields

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "code": code,
                "message": message,
            }
        )
        if exception is not UNSET:
            field_dict["exception"] = exception
        if fields is not UNSET:
            field_dict["fields"] = fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        code = d.pop("code")

        message = d.pop("message")

        exception = []
        _exception = d.pop("exception", UNSET)
        for exception_item_data in _exception or []:
            exception_item = ErrorExceptionItem.from_dict(exception_item_data)

            exception.append(exception_item)

        fields = d.pop("fields", UNSET)

        error = cls(
            code=code,
            message=message,
            exception=exception,
            fields=fields,
        )

        error.additional_properties = d
        return error

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
