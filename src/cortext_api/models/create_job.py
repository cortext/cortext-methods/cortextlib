from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.create_job_form import CreateJobForm
from typing import Dict


T = TypeVar("T", bound="CreateJob")


@attr.s(auto_attribs=True)
class CreateJob:
    """  """

    user_id: Union[Unset, int] = UNSET
    script_id: Union[Unset, int] = UNSET
    corpus_id: Union[Unset, int] = UNSET
    context: Union[Unset, str] = UNSET
    callback_url: Union[Unset, str] = UNSET
    job_name: Union[Unset, str] = UNSET
    form: Union[Unset, CreateJobForm] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        user_id = self.user_id
        script_id = self.script_id
        corpus_id = self.corpus_id
        context = self.context
        callback_url = self.callback_url
        job_name = self.job_name
        form: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.form, Unset):
            form = self.form.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if script_id is not UNSET:
            field_dict["script_id"] = script_id
        if corpus_id is not UNSET:
            field_dict["corpus_id"] = corpus_id
        if context is not UNSET:
            field_dict["context"] = context
        if callback_url is not UNSET:
            field_dict["callback_url"] = callback_url
        if job_name is not UNSET:
            field_dict["job_name"] = job_name
        if form is not UNSET:
            field_dict["form"] = form

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        user_id = d.pop("user_id", UNSET)

        script_id = d.pop("script_id", UNSET)

        corpus_id = d.pop("corpus_id", UNSET)

        context = d.pop("context", UNSET)

        callback_url = d.pop("callback_url", UNSET)

        job_name = d.pop("job_name", UNSET)

        _form = d.pop("form", UNSET)
        form: Union[Unset, CreateJobForm]
        if isinstance(_form, Unset):
            form = UNSET
        else:
            form = CreateJobForm.from_dict(_form)

        create_job = cls(
            user_id=user_id,
            script_id=script_id,
            corpus_id=corpus_id,
            context=context,
            callback_url=callback_url,
            job_name=job_name,
            form=form,
        )

        create_job.additional_properties = d
        return create_job

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
