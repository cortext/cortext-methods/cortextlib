from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.get_scripts_script_id_response_200_script_inputs_item import GetScriptsScriptIdResponse200ScriptInputsItem
from ..models.get_scripts_script_id_response_200_script_outputs_item import (
    GetScriptsScriptIdResponse200ScriptOutputsItem,
)
from ..models.get_scripts_script_id_response_200_script_require_item import (
    GetScriptsScriptIdResponse200ScriptRequireItem,
)
from typing import Dict
from typing import cast, List


T = TypeVar("T", bound="GetScriptsScriptIdResponse200Script")


@attr.s(auto_attribs=True)
class GetScriptsScriptIdResponse200Script:
    """  """

    name: Union[Unset, str] = UNSET
    desc: Union[Unset, str] = UNSET
    type: Union[Unset, str] = UNSET
    author: Union[Unset, str] = UNSET
    tags: Union[Unset, List[str]] = UNSET
    require: Union[Unset, List[GetScriptsScriptIdResponse200ScriptRequireItem]] = UNSET
    documentation: Union[Unset, str] = UNSET
    inputs: Union[Unset, List[GetScriptsScriptIdResponse200ScriptInputsItem]] = UNSET
    outputs: Union[Unset, List[GetScriptsScriptIdResponse200ScriptOutputsItem]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name
        desc = self.desc
        type = self.type
        author = self.author
        tags: Union[Unset, List[str]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = self.tags

        require: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.require, Unset):
            require = []
            for require_item_data in self.require:
                require_item = require_item_data.to_dict()

                require.append(require_item)

        documentation = self.documentation
        inputs: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.inputs, Unset):
            inputs = []
            for inputs_item_data in self.inputs:
                inputs_item = inputs_item_data.to_dict()

                inputs.append(inputs_item)

        outputs: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.outputs, Unset):
            outputs = []
            for outputs_item_data in self.outputs:
                outputs_item = outputs_item_data.to_dict()

                outputs.append(outputs_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if desc is not UNSET:
            field_dict["desc"] = desc
        if type is not UNSET:
            field_dict["type"] = type
        if author is not UNSET:
            field_dict["author"] = author
        if tags is not UNSET:
            field_dict["tags"] = tags
        if require is not UNSET:
            field_dict["require"] = require
        if documentation is not UNSET:
            field_dict["documentation"] = documentation
        if inputs is not UNSET:
            field_dict["inputs"] = inputs
        if outputs is not UNSET:
            field_dict["outputs"] = outputs

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name", UNSET)

        desc = d.pop("desc", UNSET)

        type = d.pop("type", UNSET)

        author = d.pop("author", UNSET)

        tags = cast(List[str], d.pop("tags", UNSET))

        require = []
        _require = d.pop("require", UNSET)
        for require_item_data in _require or []:
            require_item = GetScriptsScriptIdResponse200ScriptRequireItem.from_dict(require_item_data)

            require.append(require_item)

        documentation = d.pop("documentation", UNSET)

        inputs = []
        _inputs = d.pop("inputs", UNSET)
        for inputs_item_data in _inputs or []:
            inputs_item = GetScriptsScriptIdResponse200ScriptInputsItem.from_dict(inputs_item_data)

            inputs.append(inputs_item)

        outputs = []
        _outputs = d.pop("outputs", UNSET)
        for outputs_item_data in _outputs or []:
            outputs_item = GetScriptsScriptIdResponse200ScriptOutputsItem.from_dict(outputs_item_data)

            outputs.append(outputs_item)

        get_scripts_script_id_response_200_script = cls(
            name=name,
            desc=desc,
            type=type,
            author=author,
            tags=tags,
            require=require,
            documentation=documentation,
            inputs=inputs,
            outputs=outputs,
        )

        get_scripts_script_id_response_200_script.additional_properties = d
        return get_scripts_script_id_response_200_script

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
