from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.post_files_response_200_context import PostFilesResponse200Context
from typing import Dict


T = TypeVar("T", bound="PostFilesResponse200")


@attr.s(auto_attribs=True)
class PostFilesResponse200:
    """  """

    success: Union[Unset, bool] = UNSET
    id: Union[Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    document: Union[Unset, str] = UNSET
    context: Union[Unset, PostFilesResponse200Context] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        success = self.success
        id = self.id
        url = self.url
        document = self.document
        context: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.context, Unset):
            context = self.context.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if success is not UNSET:
            field_dict["success"] = success
        if id is not UNSET:
            field_dict["id"] = id
        if url is not UNSET:
            field_dict["url"] = url
        if document is not UNSET:
            field_dict["document"] = document
        if context is not UNSET:
            field_dict["context"] = context

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        success = d.pop("success", UNSET)

        id = d.pop("id", UNSET)

        url = d.pop("url", UNSET)

        document = d.pop("document", UNSET)

        _context = d.pop("context", UNSET)
        context: Union[Unset, PostFilesResponse200Context]
        if isinstance(_context, Unset):
            context = UNSET
        else:
            context = PostFilesResponse200Context.from_dict(_context)

        post_files_response_200 = cls(
            success=success,
            id=id,
            url=url,
            document=document,
            context=context,
        )

        post_files_response_200.additional_properties = d
        return post_files_response_200

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
