from enum import Enum


class StatsItemFormatsValue(str, Enum):
    VAL = "VAL"
    JSON = "JSON"

    def __str__(self) -> str:
        return str(self.value)
