from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from ..models.job_state import JobState
from ..types import UNSET, Unset


T = TypeVar("T", bound="Job")


@attr.s(auto_attribs=True)
class Job:
    """  """

    id: Union[Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    results_hash: Union[Unset, str] = UNSET
    job_user_name: Union[Unset, str] = UNSET
    result_url: Union[Unset, str] = UNSET
    document_url: Union[Unset, str] = UNSET
    state: Union[Unset, JobState] = UNSET
    user_id: Union[Unset, int] = UNSET
    script_id: Union[Unset, int] = UNSET
    corpus_id: Union[Unset, int] = UNSET
    created_at: Union[Unset, str] = UNSET
    updated_at: Union[Unset, str] = UNSET
    context: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        label = self.label
        results_hash = self.results_hash
        job_user_name = self.job_user_name
        result_url = self.result_url
        document_url = self.document_url
        state: Union[Unset, int] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        user_id = self.user_id
        script_id = self.script_id
        corpus_id = self.corpus_id
        created_at = self.created_at
        updated_at = self.updated_at
        context = self.context

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if label is not UNSET:
            field_dict["label"] = label
        if results_hash is not UNSET:
            field_dict["resultsHash"] = results_hash
        if job_user_name is not UNSET:
            field_dict["jobUserName"] = job_user_name
        if result_url is not UNSET:
            field_dict["result_url"] = result_url
        if document_url is not UNSET:
            field_dict["document_url"] = document_url
        if state is not UNSET:
            field_dict["state"] = state
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if script_id is not UNSET:
            field_dict["script_id"] = script_id
        if corpus_id is not UNSET:
            field_dict["corpus_id"] = corpus_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if context is not UNSET:
            field_dict["context"] = context

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        label = d.pop("label", UNSET)

        results_hash = d.pop("resultsHash", UNSET)

        job_user_name = d.pop("jobUserName", UNSET)

        result_url = d.pop("result_url", UNSET)

        document_url = d.pop("document_url", UNSET)

        _state = d.pop("state", UNSET)
        state: Union[Unset, JobState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = JobState(_state)

        user_id = d.pop("user_id", UNSET)

        script_id = d.pop("script_id", UNSET)

        corpus_id = d.pop("corpus_id", UNSET)

        created_at = d.pop("created_at", UNSET)

        updated_at = d.pop("updated_at", UNSET)

        context = d.pop("context", UNSET)

        job = cls(
            id=id,
            label=label,
            results_hash=results_hash,
            job_user_name=job_user_name,
            result_url=result_url,
            document_url=document_url,
            state=state,
            user_id=user_id,
            script_id=script_id,
            corpus_id=corpus_id,
            created_at=created_at,
            updated_at=updated_at,
            context=context,
        )

        job.additional_properties = d
        return job

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
