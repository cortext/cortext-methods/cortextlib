from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from typing import Dict
from ..models.get_scripts_script_id_response_200_script import GetScriptsScriptIdResponse200Script
from ..models.get_scripts_script_id_response_200_section import GetScriptsScriptIdResponse200Section


T = TypeVar("T", bound="GetScriptsScriptIdResponse200")


@attr.s(auto_attribs=True)
class GetScriptsScriptIdResponse200:
    """  """

    script: Union[Unset, GetScriptsScriptIdResponse200Script] = UNSET
    section: Union[Unset, GetScriptsScriptIdResponse200Section] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        script: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.script, Unset):
            script = self.script.to_dict()

        section: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.section, Unset):
            section = self.section.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if script is not UNSET:
            field_dict["script"] = script
        if section is not UNSET:
            field_dict["section"] = section

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _script = d.pop("script", UNSET)
        script: Union[Unset, GetScriptsScriptIdResponse200Script]
        if isinstance(_script, Unset):
            script = UNSET
        else:
            script = GetScriptsScriptIdResponse200Script.from_dict(_script)

        _section = d.pop("section", UNSET)
        section: Union[Unset, GetScriptsScriptIdResponse200Section]
        if isinstance(_section, Unset):
            section = UNSET
        else:
            section = GetScriptsScriptIdResponse200Section.from_dict(_section)

        get_scripts_script_id_response_200 = cls(
            script=script,
            section=section,
        )

        get_scripts_script_id_response_200.additional_properties = d
        return get_scripts_script_id_response_200

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
