from enum import IntEnum


class JobState(IntEnum):
    VALUE_1 = 1
    VALUE_2 = 2
    VALUE_3 = 3
    VALUE_4 = 4
    VALUE_8 = 8
    VALUE_9 = 9

    def __str__(self) -> str:
        return str(self.value)
