from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.document_context import DocumentContext
from typing import Dict
from typing import cast, List


T = TypeVar("T", bound="Document")


@attr.s(auto_attribs=True)
class Document:
    """  """

    user: Union[Unset, str] = UNSET
    id: Union[Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    context: Union[Unset, DocumentContext] = UNSET
    date: Union[Unset, str] = UNSET
    time: Union[Unset, str] = UNSET
    timestamp: Union[Unset, str] = UNSET
    size: Union[Unset, str] = UNSET
    type: Union[Unset, str] = UNSET
    filename: Union[Unset, str] = UNSET
    name: Union[Unset, str] = UNSET
    extension: Union[Unset, str] = UNSET
    files: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        user = self.user
        id = self.id
        url = self.url
        context: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.context, Unset):
            context = self.context.to_dict()

        date = self.date
        time = self.time
        timestamp = self.timestamp
        size = self.size
        type = self.type
        filename = self.filename
        name = self.name
        extension = self.extension
        files: Union[Unset, List[str]] = UNSET
        if not isinstance(self.files, Unset):
            files = self.files

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if user is not UNSET:
            field_dict["user"] = user
        if id is not UNSET:
            field_dict["id"] = id
        if url is not UNSET:
            field_dict["url"] = url
        if context is not UNSET:
            field_dict["context"] = context
        if date is not UNSET:
            field_dict["date"] = date
        if time is not UNSET:
            field_dict["time"] = time
        if timestamp is not UNSET:
            field_dict["timestamp"] = timestamp
        if size is not UNSET:
            field_dict["size"] = size
        if type is not UNSET:
            field_dict["type"] = type
        if filename is not UNSET:
            field_dict["filename"] = filename
        if name is not UNSET:
            field_dict["name"] = name
        if extension is not UNSET:
            field_dict["extension"] = extension
        if files is not UNSET:
            field_dict["files"] = files

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        user = d.pop("user", UNSET)

        id = d.pop("id", UNSET)

        url = d.pop("url", UNSET)

        _context = d.pop("context", UNSET)
        context: Union[Unset, DocumentContext]
        if isinstance(_context, Unset):
            context = UNSET
        else:
            context = DocumentContext.from_dict(_context)

        date = d.pop("date", UNSET)

        time = d.pop("time", UNSET)

        timestamp = d.pop("timestamp", UNSET)

        size = d.pop("size", UNSET)

        type = d.pop("type", UNSET)

        filename = d.pop("filename", UNSET)

        name = d.pop("name", UNSET)

        extension = d.pop("extension", UNSET)

        files = cast(List[str], d.pop("files", UNSET))

        document = cls(
            user=user,
            id=id,
            url=url,
            context=context,
            date=date,
            time=time,
            timestamp=timestamp,
            size=size,
            type=type,
            filename=filename,
            name=name,
            extension=extension,
            files=files,
        )

        document.additional_properties = d
        return document

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
