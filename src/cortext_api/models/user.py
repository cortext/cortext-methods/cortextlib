from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Optional
from typing import Union
from ..types import UNSET, Unset


T = TypeVar("T", bound="User")


@attr.s(auto_attribs=True)
class User:
    """  """

    id: Union[Unset, int] = UNSET
    email: Union[Unset, str] = UNSET
    roles: Union[Unset, str] = UNSET
    time_created: Union[Unset, int] = UNSET
    user_id: Union[Unset, int] = UNSET
    description: Union[Unset, None, str] = UNSET
    website: Union[Unset, None, str] = UNSET
    birthdate: Union[Unset, None, str] = UNSET
    last_connexion: Union[Unset, str] = UNSET
    city: Union[Unset, str] = UNSET
    country: Union[Unset, str] = UNSET
    institution: Union[Unset, None, str] = UNSET
    activity_domain: Union[Unset, None, str] = UNSET
    research_domain: Union[Unset, None, str] = UNSET
    authorization: Union[Unset, str] = UNSET
    updated_at: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        email = self.email
        roles = self.roles
        time_created = self.time_created
        user_id = self.user_id
        description = self.description
        website = self.website
        birthdate = self.birthdate
        last_connexion = self.last_connexion
        city = self.city
        country = self.country
        institution = self.institution
        activity_domain = self.activity_domain
        research_domain = self.research_domain
        authorization = self.authorization
        updated_at = self.updated_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if email is not UNSET:
            field_dict["email"] = email
        if roles is not UNSET:
            field_dict["roles"] = roles
        if time_created is not UNSET:
            field_dict["time_created"] = time_created
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if description is not UNSET:
            field_dict["description"] = description
        if website is not UNSET:
            field_dict["website"] = website
        if birthdate is not UNSET:
            field_dict["birthdate"] = birthdate
        if last_connexion is not UNSET:
            field_dict["last_connexion"] = last_connexion
        if city is not UNSET:
            field_dict["city"] = city
        if country is not UNSET:
            field_dict["country"] = country
        if institution is not UNSET:
            field_dict["institution"] = institution
        if activity_domain is not UNSET:
            field_dict["activity_domain"] = activity_domain
        if research_domain is not UNSET:
            field_dict["research_domain"] = research_domain
        if authorization is not UNSET:
            field_dict["authorization"] = authorization
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        email = d.pop("email", UNSET)

        roles = d.pop("roles", UNSET)

        time_created = d.pop("time_created", UNSET)

        user_id = d.pop("user_id", UNSET)

        description = d.pop("description", UNSET)

        website = d.pop("website", UNSET)

        birthdate = d.pop("birthdate", UNSET)

        last_connexion = d.pop("last_connexion", UNSET)

        city = d.pop("city", UNSET)

        country = d.pop("country", UNSET)

        institution = d.pop("institution", UNSET)

        activity_domain = d.pop("activity_domain", UNSET)

        research_domain = d.pop("research_domain", UNSET)

        authorization = d.pop("authorization", UNSET)

        updated_at = d.pop("updated_at", UNSET)

        user = cls(
            id=id,
            email=email,
            roles=roles,
            time_created=time_created,
            user_id=user_id,
            description=description,
            website=website,
            birthdate=birthdate,
            last_connexion=last_connexion,
            city=city,
            country=country,
            institution=institution,
            activity_domain=activity_domain,
            research_domain=research_domain,
            authorization=authorization,
            updated_at=updated_at,
        )

        user.additional_properties = d
        return user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
