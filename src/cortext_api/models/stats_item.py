from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import cast, Union
from typing import Union
from ..types import UNSET, Unset
from ..models.stats_item_formats_value import StatsItemFormatsValue


T = TypeVar("T", bound="StatsItem")


@attr.s(auto_attribs=True)
class StatsItem:
    """  """

    id: Union[Unset, int] = UNSET
    id_stat: Union[Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    updated_at: Union[Unset, str] = UNSET
    value: Union[Unset, int, str] = UNSET
    formats_value: Union[Unset, StatsItemFormatsValue] = UNSET
    name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        id_stat = self.id_stat
        label = self.label
        updated_at = self.updated_at
        value: Union[Unset, int, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        formats_value: Union[Unset, str] = UNSET
        if not isinstance(self.formats_value, Unset):
            formats_value = self.formats_value.value

        name = self.name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if id_stat is not UNSET:
            field_dict["id_stat"] = id_stat
        if label is not UNSET:
            field_dict["label"] = label
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if value is not UNSET:
            field_dict["value"] = value
        if formats_value is not UNSET:
            field_dict["Formats_value"] = formats_value
        if name is not UNSET:
            field_dict["name"] = name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        id_stat = d.pop("id_stat", UNSET)

        label = d.pop("label", UNSET)

        updated_at = d.pop("updated_at", UNSET)

        def _parse_value(data: object) -> Union[Unset, int, str]:
            if isinstance(data, Unset):
                return data
            return cast(Union[Unset, int, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _formats_value = d.pop("Formats_value", UNSET)
        formats_value: Union[Unset, StatsItemFormatsValue]
        if isinstance(_formats_value, Unset):
            formats_value = UNSET
        else:
            formats_value = StatsItemFormatsValue(_formats_value)

        name = d.pop("name", UNSET)

        stats_item = cls(
            id=id,
            id_stat=id_stat,
            label=label,
            updated_at=updated_at,
            value=value,
            formats_value=formats_value,
            name=name,
        )

        stats_item.additional_properties = d
        return stats_item

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
