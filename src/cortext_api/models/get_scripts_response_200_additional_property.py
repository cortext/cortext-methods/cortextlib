from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from ..types import UNSET, Unset


T = TypeVar("T", bound="GetScriptsResponse200AdditionalProperty")


@attr.s(auto_attribs=True)
class GetScriptsResponse200AdditionalProperty:
    """  """

    id: Union[Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    script_path: Union[Unset, str] = UNSET
    user_id: Union[Unset, int] = UNSET
    ispublic: Union[Unset, int] = UNSET
    comment: Union[Unset, str] = UNSET
    script_type_id: Union[Unset, int] = UNSET
    type: Union[Unset, str] = UNSET
    t_id: Union[Unset, int] = UNSET
    slug: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        label = self.label
        script_path = self.script_path
        user_id = self.user_id
        ispublic = self.ispublic
        comment = self.comment
        script_type_id = self.script_type_id
        type = self.type
        t_id = self.t_id
        slug = self.slug

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if label is not UNSET:
            field_dict["label"] = label
        if script_path is not UNSET:
            field_dict["script_path"] = script_path
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if ispublic is not UNSET:
            field_dict["ispublic"] = ispublic
        if comment is not UNSET:
            field_dict["comment"] = comment
        if script_type_id is not UNSET:
            field_dict["script_type_id"] = script_type_id
        if type is not UNSET:
            field_dict["type"] = type
        if t_id is not UNSET:
            field_dict["t_id"] = t_id
        if slug is not UNSET:
            field_dict["slug"] = slug

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        label = d.pop("label", UNSET)

        script_path = d.pop("script_path", UNSET)

        user_id = d.pop("user_id", UNSET)

        ispublic = d.pop("ispublic", UNSET)

        comment = d.pop("comment", UNSET)

        script_type_id = d.pop("script_type_id", UNSET)

        type = d.pop("type", UNSET)

        t_id = d.pop("t_id", UNSET)

        slug = d.pop("slug", UNSET)

        get_scripts_response_200_additional_property = cls(
            id=id,
            label=label,
            script_path=script_path,
            user_id=user_id,
            ispublic=ispublic,
            comment=comment,
            script_type_id=script_type_id,
            type=type,
            t_id=t_id,
            slug=slug,
        )

        get_scripts_response_200_additional_property.additional_properties = d
        return get_scripts_response_200_additional_property

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
