from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from typing import cast
from ..types import UNSET, Unset
from ..models.doclist_context import DoclistContext
import datetime
from typing import Dict
from dateutil.parser import isoparse


T = TypeVar("T", bound="Doclist")


@attr.s(auto_attribs=True)
class Doclist:
    """  """

    id: Union[Unset, str] = UNSET
    category: Union[Unset, str] = UNSET
    filename: Union[Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    date: Union[Unset, datetime.date] = UNSET
    time: Union[Unset, str] = UNSET
    timestamp: Union[Unset, str] = UNSET
    size: Union[Unset, str] = UNSET
    type: Union[Unset, str] = UNSET
    extension: Union[Unset, str] = UNSET
    icon: Union[Unset, str] = UNSET
    context: Union[Unset, DoclistContext] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        category = self.category
        filename = self.filename
        url = self.url
        date: Union[Unset, str] = UNSET
        if not isinstance(self.date, Unset):
            date = self.date.isoformat()

        time = self.time
        timestamp = self.timestamp
        size = self.size
        type = self.type
        extension = self.extension
        icon = self.icon
        context: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.context, Unset):
            context = self.context.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if category is not UNSET:
            field_dict["category"] = category
        if filename is not UNSET:
            field_dict["filename"] = filename
        if url is not UNSET:
            field_dict["url"] = url
        if date is not UNSET:
            field_dict["date"] = date
        if time is not UNSET:
            field_dict["time"] = time
        if timestamp is not UNSET:
            field_dict["timestamp"] = timestamp
        if size is not UNSET:
            field_dict["size"] = size
        if type is not UNSET:
            field_dict["type"] = type
        if extension is not UNSET:
            field_dict["extension"] = extension
        if icon is not UNSET:
            field_dict["icon"] = icon
        if context is not UNSET:
            field_dict["context"] = context

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        category = d.pop("category", UNSET)

        filename = d.pop("filename", UNSET)

        url = d.pop("url", UNSET)

        _date = d.pop("date", UNSET)
        date: Union[Unset, datetime.date]
        if isinstance(_date, Unset):
            date = UNSET
        else:
            date = isoparse(_date).date()

        time = d.pop("time", UNSET)

        timestamp = d.pop("timestamp", UNSET)

        size = d.pop("size", UNSET)

        type = d.pop("type", UNSET)

        extension = d.pop("extension", UNSET)

        icon = d.pop("icon", UNSET)

        _context = d.pop("context", UNSET)
        context: Union[Unset, DoclistContext]
        if isinstance(_context, Unset):
            context = UNSET
        else:
            context = DoclistContext.from_dict(_context)

        doclist = cls(
            id=id,
            category=category,
            filename=filename,
            url=url,
            date=date,
            time=time,
            timestamp=timestamp,
            size=size,
            type=type,
            extension=extension,
            icon=icon,
            context=context,
        )

        doclist.additional_properties = d
        return doclist

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
