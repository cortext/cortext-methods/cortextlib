from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from typing import Union
from ..types import UNSET, Unset
from typing import cast, List


T = TypeVar("T", bound="Log")


@attr.s(auto_attribs=True)
class Log:
    """  """

    logfile: Union[Unset, str] = UNSET
    paramsfile: Union[Unset, str] = UNSET
    std_out: Union[Unset, List[str]] = UNSET
    std_err: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        logfile = self.logfile
        paramsfile = self.paramsfile
        std_out: Union[Unset, List[str]] = UNSET
        if not isinstance(self.std_out, Unset):
            std_out = self.std_out

        std_err: Union[Unset, List[str]] = UNSET
        if not isinstance(self.std_err, Unset):
            std_err = self.std_err

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if logfile is not UNSET:
            field_dict["logfile"] = logfile
        if paramsfile is not UNSET:
            field_dict["paramsfile"] = paramsfile
        if std_out is not UNSET:
            field_dict["stdOut"] = std_out
        if std_err is not UNSET:
            field_dict["stdErr"] = std_err

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        logfile = d.pop("logfile", UNSET)

        paramsfile = d.pop("paramsfile", UNSET)

        std_out = cast(List[str], d.pop("stdOut", UNSET))

        std_err = cast(List[str], d.pop("stdErr", UNSET))

        log = cls(
            logfile=logfile,
            paramsfile=paramsfile,
            std_out=std_out,
            std_err=std_err,
        )

        log.additional_properties = d
        return log

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
