from typing import Any, Dict, Type, TypeVar, Tuple, Optional, BinaryIO, TextIO

from typing import List


import attr

from ..types import UNSET, Unset

from ..models.get_scripts_response_200_additional_property import GetScriptsResponse200AdditionalProperty
from typing import cast
from typing import Dict


T = TypeVar("T", bound="GetScriptsResponse200")


@attr.s(auto_attribs=True)
class GetScriptsResponse200:
    """  """

    additional_properties: Dict[str, GetScriptsResponse200AdditionalProperty] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = prop.to_dict()

        field_dict.update({})

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        get_scripts_response_200 = cls()

        additional_properties = {}
        for prop_name, prop_dict in d.items():
            additional_property = GetScriptsResponse200AdditionalProperty.from_dict(prop_dict)

            additional_properties[prop_name] = additional_property

        get_scripts_response_200.additional_properties = additional_properties
        return get_scripts_response_200

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> GetScriptsResponse200AdditionalProperty:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: GetScriptsResponse200AdditionalProperty) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
