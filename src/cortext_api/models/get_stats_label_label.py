from enum import Enum


class GetStatsLabelLabel(str, Enum):
    UPDATENBJOB = "updateNbJob"
    UPDATENBUSER = "updateNbUser"
    UPDATENBSCRIPTBYTYPE = "updateNbScriptByType"
    UPDATENBJOBBYTYPE = "updateNbJobByType"
    UPDATENBUSERBYCOUNTRY = "updateNbUserbyCountry"
    UPDATELASTDWY = "updateLastDwy"
    UPDATEUSERLASTDAY = "updateUserLastDay"
    UPDATEUSERLASTWEEK = "updateUserLastWeek"
    UPDATEUSERLASTYEAR = "updateUserLastYear"
    UPDATENBPROJBUSER = "updateNbProjBUser"
    UPDATENBJOBBSCRIPT = "updateNbJobBScript"
    UPDATENBJOBBYTIME = "updateNbJobByTime"
    UPDATETIMECREATEDUSER = "updateTimeCreatedUser"
    UPDATENBUSERBYTIME = "updateNbUserByTime"
    UPDATEMAXPROJBUSER = "updateMaxProjBUser"
    UPDATEDISKSPACE = "updateDiskSpace"
    UPDATENBDOC = "updateNbDoc"
    UPDATENBJOBERROR = "updateNbJobError"

    def __str__(self) -> str:
        return str(self.value)
