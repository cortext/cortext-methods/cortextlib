from collections import OrderedDict
import json
from pathlib import Path

import yaml
import cortext_api
from cortext_api.api.jobs import get_jobs_job_id

from .analysis import API_URL


def _get_params(source):
    """
    Get job parameters and separate them into system and user parts.

    (source: str, dict)

    If `source` is a string ending in '.yaml' or '.yml' or '.json', load the file.
    If `source` is a string with a job id, get the parametrs through the API.
    If `source` is a dict, use that as parameters.
    """
    if isinstance(source, str):
        if source.rsplit(".", 1)[-1:] in (["yaml"], ["yml"], ["json"]):
            source_path = Path(source).resolve()
            if source_path.suffix in (".yaml", ".yml"):
                with source_path.open() as f:
                    job_params = yaml.load(f, Loader=yaml.SafeLoader)
            elif source_path.suffix == ".json":
                with source_path.open() as f:
                    job_params = json.load(f)
        else:
            try:
                job_id = int(source)
            except ValueError:
                raise ValueError("Parameter `source` is not a config file or job id.")
            client = cortext_api.Client(base_url=API_URL)
            # Placeholder code, untested (see analysis.py for a working example)
            job_params = get_jobs_job_id.sync(client=client, job_id=job_id)
    elif isinstance(source, dict):
        job_params = source
    else:
        raise ValueError("Parameter `source` must be of type `string` or `dict`")

    return _split_params(job_params)


def _split_params(job_params):
    """
    Ensure system and user params are split (until fixed upstream in Cortext Manager).
    """
    param_groups = ["system", "user"]
    if set(job_params) == set(param_groups):
        return [job_params[k] for k in param_groups]

    system_params_keys = {
        "job_id",
        "label",
        "project_id",
        "user_id",
        "context",
        "script_path",
        "script_id",
        "corpus_file",
        "corpus_id",
        "corpus_name",
        "result_path",
        "created_at",
    }
    sys_params = {k: v for k, v in job_params.items() if k in system_params_keys}
    usr_params = {k: v for k, v in job_params.items() if k not in system_params_keys}
    usr_params.pop("container", None)
    usr_params.pop("cortextlib_needs_network", None)

    return sys_params, usr_params


def _log_params(user_params, method_descriptor_path):
    """
    This code was copied from 'cortext-methods/pylibrary/fonctions.py' as is.
    We ought to refactor it someday.
    """

    def lazyparse(yamlfile):
        feed = open(yamlfile)
        text = feed.read()
        lignes = text.split("\nsection:")[1].split("\n")
        sections = []
        sectionnames = []
        sections_params = {}
        conditions = {}
        for i, ligne in enumerate(lignes):
            if ligne[:6] == "  sect":
                # newsection

                # if ligne[:len("    label:")]=="    label:":
                section = ligne.strip()[:-1]
                sections.append(section)
            if ligne[: len("    label:")] == "    label:":
                sectionnames.append(ligne.split("label:")[1].strip())
            if ligne[: len("        label:")] == "        label:":
                param = ligne.split("label:")[1].strip()
                param = lignes[i - 1].replace(":", "").strip()
                sections_params.setdefault(section, []).append(param)
            if ligne[: len("        parent:")] == "        parent:":
                conditionnalfield = ligne.split("parent:")[1].strip()
            if ligne[: len("        value:")] == "        value:":
                conditionnal = ligne.split("value:")[1].strip()
                conditions[param] = (conditionnalfield, conditionnal)
        feed.close()
        return sections, sections_params, conditions, sectionnames

    def setup_yaml():

        # """ http://stackoverflow.com/a/8661021 """
        def represent_dict_order(self, data):
            return self.represent_mapping("tag:yaml.org,2002:map", list(data.items()))

        yaml.add_representer(OrderedDict, represent_dict_order)

    def convert_yaml_clause(reponse):
        if reponse is None:
            return False
        elif reponse == "yes":
            return True
        elif reponse == "no":
            return False
        else:
            return reponse

    def getuserparameters(yamlfile, parameters_user):
        parameterslog = {}
        sections, sections_params, conditions, sectionnames = lazyparse(yamlfile)
        yamldata = yaml.load(open(yamlfile), Loader=yaml.FullLoader)
        # print "retrieve good names"
        # for par,(conditionnalfield,conditionnal) in conditions.iteritems():
        # 	print conditionnalfield,parameterslog[yamldata['section'][section]['label']][
        #                 conditionnalfield]
        # print 'conditions',conditions
        # print 'yamldata',yamldata
        ordered_param = {}
        for section in sections:  # yamldata['section'].keys()
            # print "looking at section",section
            # for variable in sections_params[section]:#yamldata['section'].keys()
            # print "searching for variable", variable
            # print 'variable',variable
            parameterslog[yamldata["section"][section]["label"]] = {}
            # print yamldata['section'][variable]['params']
            for param in sections_params[section]:
                # print 'paramhere',param
                # print yamldata['section'][variable]['params'][param]
                try:
                    parameterslog[yamldata["section"][section]["label"]][
                        param
                    ] = yamldata["section"][section]["params"][param]["label"]
                    ordered_param.setdefault(
                        yamldata["section"][section]["label"], []
                    ).append(param)
                except Exception:
                    pass
        # print 'parameterslog',parameterslog
        parameterslog_natural = OrderedDict()
        # parameterslog=fonctions.getuserparameters(yamlfile)
        setup_yaml()
        # print 'sectionnames',sectionnames
        # print 'parameters_user_origi',parameters_user
        for sec in sectionnames:
            parameterslog_natural[sec] = OrderedDict()
            # for param in parameterslog[sec].keys():
            for param in ordered_param[sec]:
                # print 'param',param
                # parameterslog[sec][param]=parameters_user.get(param,None)
                if param in conditions:
                    # print "checking param",param
                    # print "conditions[param]",conditions[param]
                    # #print "parameters_user.get(param,False)",parameters_user.get(param,False)
                    # print "parameters_user.get(conditions[param][0],None)",parameters_user.get(
                    #            conditions[param][0],'jjkkjl')
                    # print ("convert_yaml_clause(parameters_user.get(conditions[param][0],None))",
                    #        convert_yaml_clause(conditions[param][1]))
                    if parameters_user.get(
                        conditions[param][0], "jjkkjl"
                    ) == convert_yaml_clause(conditions[param][1]):
                        parameterslog_natural[sec][
                            parameterslog[sec][param]
                        ] = parameters_user.get(param, False)
                    # 	#print 'parameterslog_natural',parameterslog_natural
                    # 	#print parameterslog_natural[sec][parameterslog[sec][param]]
                    # 	print '2becompared',parameters_user.get(conditions[param][0],'ksl')
                    # 	print parameterslog_natural[sec][parameterslog[sec][param]]
                    #         #=parameters_user.get(param,False)
                else:
                    parameterslog_natural[sec][
                        parameterslog[sec][param]
                    ] = parameters_user.get(param, False)
        return "\n                           " + yaml.dump(
            parameterslog_natural, default_flow_style=False
        ).replace("\n  ", "xyxyxazopie").replace(
            "xyxyxazopie", "\n                              "
        ).replace(
            "\n", "\n                           "
        )

    try:
        return getuserparameters(method_descriptor_path, user_params)
    except FileNotFoundError:
        return "Not printing parameters as config yaml not found. See debug log for raw parameters."
