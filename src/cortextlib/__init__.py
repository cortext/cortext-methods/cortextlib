# cortextlib - a Cortext Manger python module
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/cortextlib>
#
# Contributions are welcome, get in touch with the author(s).


"""
This module provides an abstraction between Cortext Methods and the underlying system.

Cortext Methods should be implemented as classes inheriting from the ABC `CortextMethod`.

The folder `example_method` contains a complete template for methods.

An `Analysis` class is provided to interact with the results of previous jobs.

Currently, paths returned throughout this module are `pathlib.Path` instances.

"""

__author__ = "Alexandre Hannud Abdo <abdo@member.fsf.org>"
__copyright__ = "Copyright 2019-2024 Alexandre Hannud Abdo"
__license__ = "GNU GPL version 3 or above"
__URL__ = "https://gitlab.com/cortext/cortext-methods/cortextlib/"

__version__ = "0.2"

import sys

# Check version so we can keep `legacy` inside cortextlib
if sys.version_info.major >= 3 and sys.version_info.minor >= 8:
    from .job import Job
    from .analysis import Analysis
    from .method import CortextMethod
    from .data import Data

    __all__ = ["Job", "Analysis", "CortextMethod", "Data"]
