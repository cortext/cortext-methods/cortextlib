##################
OLD MANUAL API
NEVER COMPLETED
NEVER TESTED
KEPT FOR REFERENCE
DO NOT USE
##################

import io
import functools
from pathlib import Path
from time import sleep
import httpx


API_SRV = (
    "http://localhost:3000/api"
    if Path("/vagrant").exists()
    else "http://lisis-srv-mgr:3000/api"
)
API_METHOD_RETRY = {
    httpx.get: 3,
    httpx.patch: 3,
}
API_RETRY_INTERVAL = 17


def _call_api(endpoint_call, request_method, request_kwargs):
    call_url = f"{API_SRV}{endpoint_call}"
    for _ in range(API_METHOD_RETRY.get(request_method, 1)):
        req = request_method(call_url, **request_kwargs)
        if req.status_code == httpx.codes.ok:
            break
        sleep(API_RETRY_INTERVAL)
    req.raise_for_status()
    return req.json()


def _endpoint(method):
    method = method.lower()
    request_method = getattr(httpx, method)

    def _endpoint(func):
        """
        Decorator that creates endpoint access functions from functions that
        take the endpoint template variables as parameters and return the
        endpoint path with parameters replaced plus the request arguments.
        """

        @functools.wrap(func)
        def wrapper(*args, **kwargs):
            endpoint_call = func(*args, **kwargs)
            if not isinstance(endpoint_call, str):
                endpoint_call, request_kwargs = endpoint_call
            return _call_api(endpoint_call, request_method, request_kwargs)

        return wrapper

    return _endpoint


# Projects


@_endpoint("GET")
def projects():
    """List of available projects"""
    return f"/projects/"


@_endpoint("GET")
def project(projectId):
    """Metadata of a specific project"""
    return f"/projects/{projectId}"


@_endpoint("GET")
def analysis(projectId, analysisId):
    """Metadata of a specific analysis"""
    return f"/project/{projectId}/analysis/{analysisId}"


# Jobs


@_endpoint("GET")
def jobs():
    """List all jobs"""
    return f"/jobs/"


@_endpoint("POST")
def job_create(jobspec):
    """Create a job"""
    return f"/jobs/", {"json": jobspec}


@_endpoint("GET")
def job(jobId):
    """Metadata of a specific job"""
    return f"/jobs/{jobId}"

@_endpoint("GET")
def job(jobId):
    """List all log of a job"""
    return f"/jobs/{jobId}/logs"


# Documents


@_endpoint("PATCH")
def document_update(documentId, metadata):
    """Upload a new file"""
    return f"/documents/{documentId}", {"json": metadata}


@_endpoint("POST")
def file_upload(file):
    """Upload a new file"""
    return f"/files/", {"files": {"file": _get_rb_file(file)}}


def _get_rb_file(file):
    """Try to get a readable, binary, file-like object from `file`."""
    if isinstance(file, io.IOBase) and file.readable() and "b" in file.mode:
        return file
    if isinstance(file, str):
        return Path(file).open("rb")
    if hasattribute(file, "open"):
        return file.open("rb")
    else:
        raise TypeError("Couldn't get a readable, binary, file-like object from `file`")
