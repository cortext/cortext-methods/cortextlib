#!/usr/bin/env python3

# cortextlib - a Cortext Manger python module
#
# Author(s):
# * Ale Abdo <abdo@member.fsf.org>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/cortextlib>
#
# Contributions are welcome, get in touch with the author(s).

"""
As long as we haven't upgraded our infrastructure, this script must be compatible with
Python 3.5. For this reason:
- We can't use the `docker` Python module
- We can't assume `dict` is ordered
- We can't use f-strings (ex. f"var is {var}")
- Do not use Python syntax introduced after 3.5
"""

import argparse
import os
import sys
import subprocess
from collections import OrderedDict
from itertools import chain, repeat
from pathlib import Path
import re

import yaml


VERBOSITY = 0


def setup_argparse():
    parser = argparse.ArgumentParser(
        description="Executes a Cortext Method in a container",
        epilog=None,
    )
    parser.add_argument(
        "job_parameters_file",
        help="file containing the method's parameters",
    )
    parser.add_argument(
        "-i",
        "--interactive",
        action="store_true",
        help="run in an interactive container",
    )
    parser.add_argument(
        "-s",
        "--shell",
        action="store_true",
        help="run a shell in the container instead of the method",
    )
    parser.add_argument(
        "-c",
        "--shell-command",
        help="runs the supplied command in a non-interactive shell",
    )
    parser.add_argument(
        "-d",
        "--debug",
        nargs="+",
        help="Launch the interactive debugger at specified locations:"
        ' "init", "run" or method-specific',
    )
    parser.add_argument(
        "-n",
        "--network",
        action="store_true",
        help="run with access to the host's network even if unset in the parameters",
    )
    parser.add_argument(
        "--share",
        action="append",
        default=[],
        help="Mount a path as read-write",
    )
    parser.add_argument(
        "-v",
        "--verbosity",
        action="count",
        default=0,
        help="Print some information during execution",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Dry-run, stops right before execution.",
    )
    args = parser.parse_args()
    if not args.shell:
        args.shell = args.shell_command is not None
    if not args.interactive:
        args.interactive = args.debug or (args.shell and args.shell_command is None)
    return args


def get_job_parameters(job_param_path):
    with job_param_path.open() as f:
        params = yaml.load(f, Loader=yaml.SafeLoader)
    params = params.get("sys_params", params)
    return params


class JobPaths:
    STORE_NAME = "documents"

    def __init__(self, params):
        # Paths must be absolute for docker to mount them, so we convert relative paths
        # based on the directory from where this script was executed.
        script_path = Path(params["script_path"]).absolute()
        db_dir = Path(params["corpus_file"]).absolute().parent
        result_path = Path(params["result_path"]).absolute()

        # Derived paths
        script_dir = script_path if script_path.is_dir() else script_path.parent
        debuglog_path = result_path / ".debug.log"
        store_dir = next(
            (
                parent
                for parent in result_path.parents
                if parent.name == self.STORE_NAME
            ),
            result_path.parent,
        )

        # Some methods must read from paths outside db_dir and store_dir
        ro_mount_paths = {}
        method_params_with_paths = [
            "forbiddennodesfile",
            "list_file",
            "equivalence_file",
            "terms_list",
            "pinggoogle",
        ]
        for param_name in method_params_with_paths:
            if param_name in params:
                path = Path(params[param_name]).absolute()
                ro_mount_paths[path] = None

        # FIXME: Sashimi needs write access to the full store
        rw_mount_paths = {}
        methods_needing_full_store_rw = ["cortext_sashimi"]
        if script_dir.name in methods_needing_full_store_rw:
            rw_mount_paths[store_dir] = None

        # Attribute the paths to be used
        self.result_path = result_path
        self.script_path = script_path
        self.script_dir = script_dir
        self.db_dir = db_dir
        self.debuglog_path = debuglog_path
        self.store_dir = store_dir
        self.ro_mount_paths = ro_mount_paths
        self.rw_mount_paths = rw_mount_paths


class HostParameters:
    def __init__(
        self, dir_cortext=None, dir_documents=None, env_host=None, env_path=None, network=None
    ):
        self.dir_cortext = dir_cortext
        self.dir_documents = dir_documents
        self.env_host = env_host
        self.env_path = env_path
        self.network = network

    def includes_env(self):
        return self.env_host and self.env_path

    def includes_dirs(self):
        return self.dir_cortext and self.dir_documents

    @staticmethod
    def from_dir_name(script_dir_name):
        if script_dir_name in {"cortext_sashimi", "openalex"}:
            return HostParameters(
                dir_cortext="/srv/lisis-prod/pool0/web/cortext",
                dir_documents="/srv/lisis-prod/pool0/data/documents",
                env_host="ssh://adminlisis@lisis-cnt-02",
                env_path="/usr/local/bin:/usr/bin:/bin",
            )

    @staticmethod
    def from_environment():
        if (
            "CORTEXT_ENV_DIR_CORTEXT" in os.environ
            and "CORTEXT_ENV_DIR_DOCUMENTS" in os.environ
            and "CORTEXT_ENV_NETWORK" in os.environ
        ):
            return HostParameters(
                dir_cortext=os.environ["CORTEXT_ENV_DIR_CORTEXT"],
                dir_documents=os.environ["CORTEXT_ENV_DIR_DOCUMENTS"],
                network=os.environ["CORTEXT_ENV_NETWORK"],
            )


def get_host_parameters(script_dir_name):
    host_params = HostParameters.from_environment()
    if not host_params:
        host_params = HostParameters.from_dir_name(script_dir_name)
    if not host_params:
        host_params = HostParameters()
    return host_params


class PathMounts:
    """
    Build paths and mount commands.
    """

    def __init__(self, job_param_path, job_paths, host_params, is_legacy):
        self.job_param_path = job_param_path
        self.job_paths = job_paths

        # Set the python path where to mount methods that use CortextMethod
        self.cortextlib_python_path = Path("/cortext/python_libs")

        # Find where to place methods
        self.methods_dir = next(
            (
                dir
                for dir in self.job_paths.script_dir.parents
                if dir.name.startswith("cortext-methods")
            ),
            self.job_paths.script_dir.parent,
        )
        # Determine where would "cortext-methods" be
        self.cortext_methods_dir = self.methods_dir.parent / "cortext-methods"

        self.host_params = host_params
        self.is_legacy = is_legacy

    def get_mounts(self):
        return self.get_base_mounts() + self.get_method_mounts()

    def get_base_mounts(self):
        base_mounts = []

        # Cortext Manager only allows script paths starting from "cortext-methods"
        base_mounts.append(self.mount_tmpfs(dest=self.cortext_methods_dir))

        # Access to the parameter file, the database, the results dir
        base_mounts.append(
            self.mount_bind(
                source=self.job_param_path, dest=self.job_param_path, readonly=False
            )
        )
        base_mounts.append(
            self.mount_bind(
                source=self.job_paths.db_dir, dest=self.job_paths.db_dir, readonly=False
            )
        )
        base_mounts.append(
            self.mount_bind(
                source=self.job_paths.result_path,
                dest=self.job_paths.result_path,
                readonly=False,
            )
        )

        # Store access
        for path in self.job_paths.ro_mount_paths:
            base_mounts.append(self.mount_bind(source=path, dest=path))
        for path in self.job_paths.rw_mount_paths:
            base_mounts.append(self.mount_bind(source=path, dest=path, readonly=False))

        # Cortext Manager in vagrant instances has 'app/../documents' in result_path
        if "documents" in self.job_paths.store_dir.parts:
            app_path = self.mount_tmpfs(
                dest=Path().joinpath(
                    *self.job_paths.store_dir.parts[
                        : self.job_paths.store_dir.parts.index("documents")
                    ],
                    "app",
                )
            )
            base_mounts.append(app_path)

        return base_mounts

    def get_method_mounts(self):
        if self.is_legacy:
            return self.get_legacy_method_mounts()
        return self.get_cortextlib_method_mounts()

    def get_cortextlib_method_mounts(self):
        return [
            # Mount the directory containing the script so __main__.py can be run
            self.mount_bind(
                source=self.job_paths.script_dir, dest=self.job_paths.script_dir
            ),
            # Mount the method's package where it can be imported
            self.mount_bind(
                source=self.job_paths.script_dir,
                dest=self.cortextlib_python_path / self.job_paths.script_dir.name,
            ),
        ]

    def get_legacy_method_mounts(self):
        legacy_mounts = []

        # Mount the directory containing the script
        legacy_mounts.append(
            self.mount_bind(
                source=self.job_paths.script_dir, dest=self.job_paths.script_dir
            )
        )

        # Required by some operations (corpus_terms_indexer, terms_extractor)
        legacy_mounts.append(
            self.mount_bind(
                source=self.methods_dir / "state" / "equivalence.txt",
                dest=self.job_paths.script_dir.parent / "state" / "equivalence.txt",
                readonly=False,
            )
        )

        # Required by some operations (terms_extractor)
        if self.job_paths.script_dir.name != "corpus-terms-indexer":
            legacy_mounts.append(
                self.mount_bind(
                    source=self.methods_dir / "corpus-terms-indexer",
                    dest=self.job_paths.script_dir.parent / "corpus-terms-indexer",
                )
            )

        # Required by some operations (sentiment-analysis)
        if self.job_paths.script_dir.name != "corpus-explorer":
            legacy_mounts.append(
                self.mount_bind(
                    source=self.methods_dir / "corpus-explorer",
                    dest=self.job_paths.script_dir.parent / "corpus-explorer",
                )
            )

        return legacy_mounts

    def mount_bind(self, *, source, dest, readonly=True):
        """
        Bind a host path to a container path, readonly by default.
        """
        if self.host_params.includes_dirs():
            source = self.get_host_path(source)
        mount_args = ["type=bind", "source={source}", "destination={dest}"]
        if readonly:
            mount_args.append("readonly")
        return ",".join(mount_args).format(source=source, dest=dest)

    @staticmethod
    def mount_tmpfs(*, dest):
        """
        Provide a an empty read-only tmpfs directory.
        """
        tmpfs_tpl = "type=tmpfs,tmpfs-size=0,tmpfs-mode=0555,destination={dest}"
        return tmpfs_tpl.format(dest=dest)

    def get_host_path(self, path: Path) -> str:
        """
        Rewrite paths to match host filsystem.
        We look for:
        - /{self.store_dir.name}/ as the cue to replace by dir:documents
        - /cortext/ as the cue to replace by dir:cortext
        """
        path = str(path)
        # cortext/ may contain store_dir (e.g. documents/) so condition
        # exclusivity and order matters
        local_doc = r".*/{store_dir}(?=/)".format(
            store_dir=self.job_paths.store_dir.name
        )
        host_doc = self.host_params.dir_documents
        if re.search(local_doc, path):
            return re.sub(local_doc, host_doc, path)
        local_ctx = ".*/cortext(?=/)"
        host_ctx = self.host_params.dir_cortext
        if re.search(local_ctx, path):
            return re.sub(local_ctx, host_ctx, path)


def get_hosts():
    hosts = [
        # Required by some operations (w2vexplorer)
        "assets.cortext.local:172.17.0.1",
        "assets.cortext.dev:172.17.0.1",
        "assets.cortext.net:172.17.0.1",
    ]
    return hosts


def get_user():
    return "{}:{}".format(os.getuid(), os.getgid())


def get_job_env(*, cortextlib_python_path, debug):
    env = {}
    pythonpaths = [cortextlib_python_path, os.environ.get("PYTHONPATH", None)]
    env["PYTHONPATH"] = ":".join(str(path) for path in pythonpaths if path is not None)
    if debug:
        env["CORTEXTLIB_DEBUG"] = debug
    return env


def get_command_env(*, host_params):
    env = os.environ.copy()
    if host_params.includes_env():
        env["DOCKER_HOST"] = host_params.env_host
        env["PATH"] = host_params.env_path
    return env


def build_docker_command(
    user,
    mounts,
    workdir,
    network,
    hosts,
    docker_image,
    job_env,
    interpreter,
    script_path,
    job_param_path,
):
    command = OrderedDict()
    command["command"] = ["docker", "run"]
    command["options"] = []
    command["options"].extend(["--rm"])
    command["options"].extend(["--hostname", "cortext-methods"])
    if not network:
        command["options"].extend(["--network", "none"])
    else:
        command["hosts"] = list(chain(*zip(repeat("--add-host"), hosts)))
        command["options"].extend(["--network", network])
    command["user"] = ["--user", user]
    command["mounts"] = list(chain(*zip(repeat("--mount"), mounts)))
    if workdir is not None:
        command["workdir"] = ["--workdir", workdir]
    command["image"] = [docker_image]
    if job_env:
        command["call_env"] = ["/usr/bin/env"] + [
            "{k}={v}".format(k=k, v=v) for k, v in job_env.items()
        ]
    command["call_cmd"] = [interpreter]
    command["call_args"] = [str(script_path), str(job_param_path)]
    return command


def launch_command(
    command, env, interactive, shell, shell_command, debuglog_path, dry_run
):
    def get_args_env(command, env):
        command_string = sum(command.values(), [])
        if VERBOSITY:
            print(env)
            print(command)
            print(" ".join(command_string))
        return {"args": command_string, "env": env}

    if interactive:
        command["options"].extend(["--interactive", "--tty"])
        command["call_cmd"].append("-i")
    if shell:
        command["call_cmd"] = ["bash"]
        command["call_args"] = [] if shell_command is None else ["-c", shell_command]

    args_env = get_args_env(command, env)
    if dry_run:
        child_process = None
    elif interactive:
        child_process = subprocess.run(**args_env)
    else:
        # Send all output to the debug-level log file
        with debuglog_path.open("a") as debuglog_f:
            child_process = subprocess.run(
                **args_env, stdout=debuglog_f, stderr=subprocess.STDOUT
            )
    return child_process


def main(
    job_param_path, interactive, shell, shell_command, debug, network, share, dry_run
):
    # Ready...
    params = get_job_parameters(job_param_path)
    job_paths = JobPaths(params)
    job_paths.rw_mount_paths.update({Path(x): None for x in share})
    # Legacy methods are not implemented with CortextMethod
    is_legacy = (
        not job_paths.script_path.is_dir()
        and job_paths.script_path.name != "__main__.py"
    )
    # Whether and how to run the script in a remote server
    host_params = get_host_parameters(job_paths.script_dir.name)

    # Set...
    mounter = PathMounts(job_param_path, job_paths, host_params, is_legacy)
    container, docker_image = params["container"].split(":")
    assert container == "docker"  # We only support docker for now
    command = build_docker_command(
        user=get_user(),
        mounts=mounter.get_mounts(),
        # Some legacy scripts must run from the script's directory
        workdir=str(job_paths.script_dir) if is_legacy else None,
        # Cortext Manager recognizes "yes"/"no" as booleans but not true/false
        network=(
            (host_params.network or "bridge")
            if (network or params.get("cortextlib_needs_network", False))
            else False
        ),
        hosts=get_hosts(),
        docker_image=docker_image,
        job_env=get_job_env(
            cortextlib_python_path=mounter.cortextlib_python_path, debug=debug
        ),
        # TODO declare interpreter in the script yaml
        interpreter="python2" if is_legacy else "python3",
        script_path=job_paths.script_path,
        job_param_path=job_param_path,
    )

    # Go!
    cmd_env = get_command_env(host_params=host_params)
    child_process = launch_command(
        command,
        cmd_env,
        interactive,
        shell,
        shell_command,
        job_paths.debuglog_path,
        dry_run,
    )
    return_code = 0 if child_process is None else child_process.returncode
    sys.exit(return_code)


def cli():
    args = setup_argparse()
    global VERBOSITY
    VERBOSITY = args.verbosity
    main(
        job_param_path=Path(args.job_parameters_file).absolute(),
        interactive=args.interactive,
        shell=args.shell,
        shell_command=args.shell_command,
        debug=args.debug,
        network=args.network,
        share=args.share,
        dry_run=args.dry_run,
    )


if __name__ == "__main__":
    cli()
