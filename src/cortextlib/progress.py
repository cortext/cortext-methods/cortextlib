import yaml
import copy

CORTEXT_MANAGER_TOTAL = 100


class Progress:
    """
    Keeps track and syncs progress with CortextManager
    """

    def __init__(self, job, total=CORTEXT_MANAGER_TOTAL):
        self._job = job
        self._progress_file_name = "progress.yml"
        self.set_total(total)
        self._data = {"progress": 0}
        self._foreign_data = None
        self._sync_data()

    def set_total(self, value):
        self._total = value

    def advance(self, value=1):
        new_progress = self._data["progress"] + value
        assert self._check_progress(new_progress)
        self._data["progress"] = new_progress
        self._sync_data()

    def update_data(self, data):
        assert "progress" in data and self._check_progress(data["progress"])
        self._data.update(data)
        self._sync_data()

    @property
    def _path(self):
        return self._job.dir / self._progress_file_name

    def _sync_data(self):
        try:  # sync'ing progress ain't essential
            data = self._data | {  # scale progress to fixed foreign total
                "progress": int(
                    CORTEXT_MANAGER_TOTAL * self._data["progress"] / self._total
                )
            }
            if data != self._foreign_data:  # only sync if data has changed
                with self._path.open("w") as f:
                    # `width` required due to and old bug in PHP yaml.
                    # ^ TODO: test if still needed?
                    yaml.safe_dump(data, f, width=float("inf"))
                self._foreign_data = copy.deepcopy(data)
        except Exception:
            if __debug__:
                raise
            self.job.logger("Error: Failed to sync progress.")

    def _check_progress(self, value):
        if not (0 <= value <= self._total):
            raise ValueError("Progress must belong in interval [0, `self._total`].")
        return True
