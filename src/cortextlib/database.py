from pathlib import Path

import yaml


class Database:
    def __init__(self, db_file, db_name=None):
        "CMv2 database file and metadata"
        self._metadata_methods_root_key = "_methods"
        self._db_file = db_file
        self._db_name = db_name

    @property
    def path(self):
        """
        Path of the database containing the corpus to be analysed.
        If method is a corpus parser, this is instead the Path of the dataset to be
        parsed.
        """
        return Path(self._db_file).resolve()

    @property
    def name(self):
        return self.path.stem if self._db_name is None else self._db_name

    @property
    def _metadata_path(self):
        """
        Path to the dabase's metadata dictionary (aka the "descriptor")
        """
        return self.path.parent / (self.name + ".yaml")

    def metadata_get_for_method_key(self, method_key):
        """
        Return the database's metadata for a given method key.

        Roughly equivalent to
        `.metadata_get_root()[self._metadata_methods_root_key][method_key]`
        """
        meta = self.metadata_get_root()
        return meta.get(self._metadata_methods_root_key, {}).get(method_key, {})

    def metadata_get_root(self):
        """
        Return the database's full metadata dictionary.
        """
        try:
            with self._metadata_path.open() as f:
                meta = yaml.load(f, Loader=yaml.SafeLoader)
        except FileNotFoundError:
            meta = {}
        if not isinstance(meta, dict):
            raise ValueError("Metadata file does not contain a `dict`.")

        return meta

    def metadata_update_for_method_key(self, method_key, updater):
        """
        Update the database's metadata for a given method_key.

        (method_key) the key for which to update method metadata
        (updater) callable applied to the metadata dictionary

        Note: The return value of ` updater` is discarded.

        Examples:
        ```
        _metadata_update_for_method_key(
             lambda meta: meta.setdefault(some_key, some_value)
        )
        _metadata_update_for_method_key(
             lambda meta: meta.setdefault(some_key, []).append(some_value)
        )
        """

        def root_updater(meta):
            method_key_meta = meta.setdefault(
                self._metadata_methods_root_key, {}
            ).setdefault(method_key, {})
            updater(method_key_meta)

        self._metadata_update_root(root_updater)

    def _metadata_update_root(self, updater):
        """
        Update the database's metadata dictionary.

        This method updates the full metadata, which is rarely intended.
        CortextMethod subclasses should prefer `metadata_get_for_method_key()`

        This method retrieves the dictionary, applies `updater` to it, then writes back
        the dictionary to disk.

        (updater) callable applied to the metadata dictionary

        Note: The return value of ` updater` is discarded.
        """
        meta = self.metadata_get_root()
        updater(meta)
        with self._metadata_path.open("w") as f:
            # `width` required due to and old bug in PHP yaml.
            # TODO: test if still needed?
            yaml.dump(meta, f, width=float("inf"))


def register_new_database(data, corpus_type=":unkown:"):
    all_fields = data.db_con.execute(
        """
        SELECT name FROM sqlite_master
        WHERE type='table'
        ORDER BY name;
        """
    ).fetchall()
    all_fields = [x[0] for x in all_fields]
    textual_fields = data.db_con.execute(
        """
        SELECT name FROM sqlite_master
        WHERE type='table'
        AND (SELECT type FROM PRAGMA_TABLE_INFO(sqlite_master.name)
             WHERE name='data')='text'
        ORDER BY name;
        """
    ).fetchall()
    textual_fields = [x[0] for x in textual_fields]
    meta = {
        "alltables": all_fields,
        "corpus_type": corpus_type,
        "extension": "db",
        "file": str(data.db.path),
        "indexed": False,
        "origin": "dataset",
        "structure": "reseaulu",
        "tablenames": all_fields.copy(),  # can CM handle yaml pointers?
        "textual_fields": textual_fields,
        "totaltables": all_fields.copy(),  # can CM handle yaml pointers?
        "uri": data.db.name,
        "version": 7,
    }
    meta_path = data.db.path.parent / (data.db.name + ".yaml")
    assert not meta_path.exists()
    with meta_path.open("w") as f:
        yaml.dump(meta, f, width=float("inf"))  # CM issue with finite width
