import sys

if sys.version_info.major < 3:
    raise RuntimeError("Should not have reached this if not running Python3")

#################################
# Cortext Method Initialization #
#################################

import importlib
import inspect
from pathlib import Path

from . import Job


def main():
    module_name = get_module_name()
    Method = importlib.import_module(module_name).Method
    parameters_file = sys.argv[1]
    job = Job(job_params=parameters_file)
    with Method(job) as method:
        method.run()


def get_module_name():
    """
    Get the module name by inspecting the frame at the top of the stack.
    Consider three cases:
    `python -m <module>`:
        get it from `f_locals["main_globals"]["__package__"]`
    `python <module>/__main__.py`:
        `__package__` is None, get it from `f_locals["__file__"]`
    `python <module>`:
        `__package__` is "", get it from `f_locals["__file__"]`
    """
    top_frame = inspect.stack()[-1].frame
    top_frame_locals = top_frame.f_locals
    main_globals = top_frame_locals.get("main_globals", top_frame_locals)
    main_package = main_globals["__package__"]
    if main_package in (None, ""):
        main_file = main_globals["__file__"]
        module_name = Path(main_file).parent.name
    else:
        module_name = main_package
    return module_name
