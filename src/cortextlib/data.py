from functools import wraps
import numpy as np
import pandas as pd
import sqlite3
import weakref

from .database import Database


class PrintLogger:
    def __getattribute__(self, x):
        return print


def commit(func):
    @wraps(func)
    def _commits(self, *args, **kwargs):
        with self.db_con:
            return func(self, *args, **kwargs)

    return _commits


class Data:
    """Handles data between Cortext method and Cortext Manager"""

    def __init__(self, db: Database, logger=PrintLogger()):
        self._db = db
        self.logger = logger
        self.logger.debug(f"Connecting to database at: '{self.db.path}'")
        self.db_con = sqlite3.connect(self.db.path)
        self._finalizer = weakref.finalize(self, self.db_con.close)
        data_tables = [x for x in self.table_names() if self.is_data_table(x)]
        nondata_tables = [x for x in self.table_names() if not self.is_data_table(x)]
        self.logger.debug(f"Data tables: {data_tables}")
        self.logger.debug(f"Non-data tables: {nondata_tables}")

    _levels = ("id", "rank", "parserank")
    table_names_sql = 'select name from sqlite_master where type="table";'
    columns_sql = "select * from {table_name} limit 0;"
    data_sql = "select {fields}, data from {table_name};"
    raw_table_sql = "select * from {table_name};"
    drop_table_sql = "drop table {table_name};"

    @property
    def db(self):
        return self._db

    @property
    def levels(self):
        """Reseaulu table column level labels"""
        return self._levels

    def table_names(self):
        return [x[0] for x in self.db_con.execute(self.table_names_sql).fetchall()]

    def is_data_table(self, table_name):
        table_columns = pd.read_sql_query(
            self.columns_sql.format(table_name=table_name), self.db_con
        ).columns
        return "id" in table_columns and "data" in table_columns

    def read_raw_table(self, table_name):
        return pd.read_sql_query(
            self.raw_table_sql.format(table_name=table_name), self.db_con
        )

    def read_data_table(self, table_name):
        data = pd.read_sql_query(
            self.data_sql.format(fields=",".join(self.levels), table_name=table_name),
            self.db_con,
        )
        return data.set_index(list(self.levels))

    def drop_table(self, table_name):
        self.db_con.execute(self.drop_table_sql.format(table_name=table_name))

    def to_dataframe(self, columns=None, only_schema=False, squeeze=True):
        """
        Read from Reseaulu db and produce a pandas DataFrame with the `id`s as index.
        """
        tables = self.table_names()
        if columns is not None:
            tables = [tn for tn in tables if tn in columns]
        data = pd.DataFrame()
        data_schema = {}
        for table_name in filter(self.is_data_table, tables):
            levels = list(self.levels)  # ["id", "rank", "parserank"]
            table_data = self.read_data_table(table_name)
            data_schema[table_name] = table_schema = []
            if squeeze:
                if not table_data.index.is_unique:
                    table_data = table_data.groupby(level=levels).agg(list)
                    table_schema.append("sequence")
                while (field := levels.pop()) != "id":
                    # Reseaulu's db format doesn't distinguish elements from
                    # lists, so we check whether we ever see nonzero values for
                    # `field` and if so treat its level as lists and inform the
                    # schema. WARNING: This implies we can't roundtrip the
                    # schema for columns that contain only 1-lists: their
                    # listness will be lost.
                    if (
                        # pandas.Series.max() gives `nan` (True) for empty series
                        not table_data.empty
                        and table_data.index.get_level_values(field).max()
                    ):
                        table_data = table_data.groupby(level=levels).agg(
                            lambda x: list(
                                y.get(idx)
                                for y in [x.droplevel(levels)]
                                for idx in range(y.index.max() + 1)
                            )
                        )
                        table_schema.append(field)
                    else:
                        table_data = table_data.droplevel(field)
            table_data = table_data.rename(columns={"data": table_name})
            if not only_schema:
                data = data.join(table_data, how="outer")
        data.name = self.db.name
        return (None if only_schema else data), data_schema

    @commit
    def from_dataframe(self, data, data_schema=None, data_source=":from_dataframe:"):
        if data_schema is None:
            _, data_schema = self.to_dataframe(columns=data.columns, only_schema=True)
        data_tables = set(x for x in self.table_names() if self.is_data_table(x))
        nondata_tables = set(x for x in self.table_names() if not self.is_data_table(x))
        for table_name in data:
            if table_name in nondata_tables:
                raise ValueError(
                    "Name `{table_name}` already in use by a non-data table."
                )
            elif table_name in data_tables:
                self.drop_table(table_name)
            create_table(table_name, get_datatype(data[table_name]), self.db_con)
        for table_name, table_data in data.items():
            table_schema = data_schema[table_name]

            def struct_iter(level, up_val):
                # Check isna and not list, bc pd.isna([None]) is True
                if not isinstance(up_val, (list, np.ndarray)) and pd.isna(up_val):
                    return []
                return enumerate(up_val) if level in table_schema else [(0, up_val)]

            for id, id_val in table_data.items():
                for rank, rank_val in struct_iter("rank", id_val):
                    for parserank, parserank_val in struct_iter("parserank", rank_val):
                        for sequence, sequence_val in struct_iter(
                            "sequence", parserank_val
                        ):
                            if pd.notna(sequence_val):
                                insert_record(
                                    table_name=table_name,
                                    source=data_source,
                                    id=id,
                                    rank=rank,
                                    parserank=parserank,
                                    data=sequence_val,
                                    db_con=self.db_con,
                                )


def create_database(db_path):
    """
    Creates an SQLite database and returns a connection.
    """
    if db_path != ":memory:":
        assert not db_path.exists()
    return sqlite3.connect(db_path)


def create_table(table_name, data_type, db_con):
    """
    Implementation note:
    Using f-string as DB-API doesn't do parameter substitution on create table.
    """
    sql = f"""
    CREATE TABLE {table_name} (
      file text,
      id integer,
      rank integer,
      parserank integer,
      data {data_type}
    );
    """
    db_con.execute(sql)


def insert_record(table_name, source, id, rank, parserank, data, db_con):
    sql = f"INSERT INTO {table_name} VALUES (:file, :id, :rank, :parserank, :data);"
    db_con.execute(
        sql, dict(file=source, id=id, rank=rank, parserank=parserank, data=data)
    )


def get_datatype(series):
    """
    Get the SQL data type name for a series
    by traversing deeper into lists until a valid scalar is found.
    """

    def first_valid(obj):
        if isinstance(obj, (list, tuple)):
            for el in obj:
                el = first_valid(el)
                notna = pd.notna(el)
                if isinstance(notna, bool) and notna:
                    return el
        else:
            return obj

    for value in series:
        value = first_valid(value)
        notna = pd.notna(value)
        if isinstance(notna, bool) and notna:
            if isinstance(value, (np.integer, int)):
                return "integer"
            if isinstance(value, (np.floating, float)):
                return "real"
            else:
                return "text"


def get_data_schema(df):
    def check_first(se):
        return not se.empty and isinstance(se.iloc[0], (list, tuple, np.ndarray))

    schema = {col: [] for col in df.columns}
    for col in df.columns:
        se = df[col].dropna()
        if check_first(se):
            schema[col].append("rank")
        se = se.explode().dropna()
        if check_first(se):
            schema[col].append("parserank")
        se = se.explode().dropna()
        if check_first(se):
            schema[col].append("sequence")
    return schema
