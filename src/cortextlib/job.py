import logging
import warnings
from pathlib import Path
import pprint

from .parameters import _get_params, _log_params
from .database import Database
from .data import Data
from .progress import Progress


# TODO: once the Cortext API is stable, `Job` should only receive a job_id and get
# everything else through the API.


class Job:
    def __init__(self, job_params):
        """
        (job_params: str or dict) a source of job parameters (see `_get_params()`)

        TODO: receive only a job_id and get job data from the API
        """
        self._sys_params, self._usr_params = _get_params(job_params)
        self._method_name = self.usr_params.pop("cortext_method", self.script_dir.name)
        self._logger = self._start_logger()
        self.logger.info(f"Job store: {self.dir}")
        self.logger.info(_log_params(self.usr_params, self.method_descriptor_path))
        self.logger.debug(f"User parameters:\n{pprint.pformat(self.usr_params)}")
        self._db = Database(
            self.sys_params["corpus_file"],
            self.sys_params.get("corpus_name", None),
        )
        self._data = None
        self._progress = None

    @property
    def logger(self):
        return self._logger

    @property
    def log(self):
        warnings.warn(
            "Cortext: Please replace `Job.log` with `Job.logger`", DeprecationWarning
        )
        return self.logger

    @property
    def sys_params(self):
        return self._sys_params

    @property
    def usr_params(self):
        return self._usr_params

    @property
    def id(self):
        return str(self.sys_params["job_id"])

    @property
    def analysis_id(self):
        return str(self.sys_params["context"]["analysis_id"])

    @property
    def project_id(self):
        return str(self.sys_params["context"]["project_id"])

    @property
    def name(self):
        return self.sys_params["label"]

    @property
    def script_dir(self):
        path = Path(self.sys_params["script_path"])
        return path if path.is_dir() else path.parent

    @property
    def method_name(self):
        return self._method_name

    @property
    def method_descriptor_path(self):
        """NOTE: this is currently an independent value in the CM database.
        In that sense, it should be provided by CM in the parameters.
        Here we're forced to guess it from the script dir."""
        return self.script_dir / f"{self.method_name}.yaml"

    @property
    def dir(self):
        return Path(self.sys_params["result_path"]).resolve()

    @property
    def db(self):
        return self._db

    @property
    def data(self):
        """
        A cortextlib.Data object for the job's database.
        This property is lazy, it instantiates the object when first called.
        """
        if self._data is None:  # lazy
            self._data = Data(self.db, self.logger)
        return self._data

    @property
    def progress(self):
        if self._progress is None:  # lazy
            self._progress = Progress(self)
        return self._progress

    def _start_logger(self):
        """
        In CortextManager:
        - 'debug' and above goes to '.debug.log'
        - 'info' and above goes into '.user.log'
        - 'warning' and above will also eventually be shown on the project dashboard
        The root logger is configured, on the assumption that it is easier to unconfigure
        the occasional problem logger than to configure every logger. This may change.
        """
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            fmt="%(asctime)s %(levelname)s : %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
        debuglog_handler = logging.FileHandler(self.dir / ".debug.log")
        debuglog_handler.setLevel(logging.DEBUG)
        debuglog_handler.setFormatter(formatter)
        root_logger.addHandler(debuglog_handler)
        userlog_handler = logging.FileHandler(self.dir / ".user.log")
        userlog_handler.setLevel(logging.INFO)
        userlog_handler.setFormatter(formatter)
        root_logger.addHandler(userlog_handler)

        logger = logging.getLogger(self.method_name)
        logger.setLevel(logging.DEBUG)

        return logger
