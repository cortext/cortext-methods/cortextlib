from abc import abstractmethod
from contextlib import AbstractContextManager
import os

from .job import Job


class CortextMethod(AbstractContextManager):
    """ABC for Cortext Methods"""

    @abstractmethod
    def __init__(self, job: Job):
        self._job = job
        self._debug = os.environ.get("CORTEXTLIB_DEBUG", "").split()
        if "init" in self._debug:
            breakpoint()

    @abstractmethod
    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.job.logger.info("Exit: job finished gracefully.")
        else:
            self.job.logger.error("Exit: job failed.")
            self.job.logger.error(f"{exc_type}: {exc_value}")
            self.job.logger.error(f"Traceback:\n{traceback}")

    @abstractmethod
    def main(self, **params):
        pass

    def run(self):
        if "run" in self._debug:
            breakpoint()
        return self.main(**self.params)

    @property
    def job(self):
        return self._job

    @property
    def data(self):
        return self.job.data

    @property
    def params(self):
        return self.job.usr_params
