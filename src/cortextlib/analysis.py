from pathlib import Path

import cortext_api
from cortext_api.api.projects import get_project_project_id_analysis_analysis_id

API_SRV_DEV = "localhost"
API_SRV_PROD = "10.1.59.221"  # FIXME: lisis-srv-mgr
API_URL = "http://{srv}:3000/api"
# API_URL = "https://monitoring.cortext.net/api"  # TODO


class Analysis:
    """
    Recover metadata and access files from a completed job.

    ```
    from cortextlib import Analysis

    previous_analysis_dir = Analysis(project_id, analysis_id).dir
    previous_output = (previous_analysis_dir / "some_output.json").read_text()
    ```
    """

    def __init__(self, pid, aid, client=None):
        if client is None:
            try:
                client = cortext_api.Client(base_url=API_URL.format(srv=API_SRV_PROD))
                self.data = get_project_project_id_analysis_analysis_id.sync(
                    client=client, project_id=pid, analysis_id=aid
                ).to_dict()

            except Exception:
                client = cortext_api.Client(base_url=API_URL.format(srv=API_SRV_DEV))
                self.data = get_project_project_id_analysis_analysis_id.sync(
                    client=client, project_id=pid, analysis_id=aid
                ).to_dict()
        else:
            # workaround for https://gitlab.com/cortext/cortext-monitoring/-/issues/14
            self.data = get_project_project_id_analysis_analysis_id.sync(
                client=client, project_id=pid, analysis_id=aid
            ).to_dict()

        assert self.id == aid and self.project_id == pid

    def __getitem__(self, key):
        return self.data[key]

    @property
    def id(self):
        return str(self.data["id"])

    @property
    def project_id(self):
        return str(self.data["project"])

    @property
    def job_id(self):
        return str(self.data["jobId"])

    @property
    def dir(self):
        return Path(self.data["parameters"]["result_path"]).resolve()

    @property
    def name(self):
        return self.data["name"]
