# coding: utf-8

"""
Simply import this module in the very beginning of the script:
```
from cortextlib.legacy import containerize
```
"""

from .container import get_script, containerize


containerize(get_script())
