# coding: utf-8

import inspect
from os import path
import socket
import subprocess
import sys


def containerize(script_file):
    """
    If it's Cortext Manager calling, call within a container using `cm-job-run`
    """
    if socket.gethostname() != "cortext-methods":
        lib_root = path.dirname(path.dirname(__file__))
        job_runner = path.sep.join([lib_root, "cm_job_run.py"])
        command = ["python3", job_runner] + sys.argv[1:]
        sys.exit(subprocess.call(command))


def get_script():
    """
    Get the script path by inspecting the frame at the top of the stack.
    Consider four cases:
    `python -m <module>`:
        get it from `f_locals["main_globals"]["__file__"]`
    `python <module>/__main__.py`
    `python <module>`
    `python <file>.py`:
        get it from `f_locals["__file__"]`
    """
    top_frame = inspect.stack()[-1][0]
    top_frame_locals = top_frame.f_locals
    main_globals = top_frame_locals.get("main_globals", top_frame_locals)
    if "__file__" not in main_globals:
        caller_name = main_globals.get("__name__", "<no __name__ found for caller>")
        raise RuntimeError("Caller `{}` has no `__file__`".format(caller_name))
    main_file = main_globals["__file__"]
    return main_file
