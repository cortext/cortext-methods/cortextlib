#! /usr/bin/env bash
set -euxo pipefail

#############################
# Setup directory structure #
#############################

# Start from a clean directory
mkdir test
cd test

LIB_DIR="lib"
mkdir $LIB_DIR


####################
# Setup cortextlib #
####################

(
    cd ${LIB_DIR}/
    git clone https://gitlab.com/cortext/cortext-methods/cortextlib.git
)

CM_JOB_RUN=$(realpath ./${LIB_DIR}/cortextlib/src/cortextlib/cm_job_run.py)


############################################
# Some methods require files sharing state #
############################################

(
    cd ${LIB_DIR}/
    mkdir state
    curl https://assets.cortext.net/docs/93c5140b84c17e7459be07bf189fdecc > \
         state/equivalence.txt
)


###################################################
# Some methods require other methods to be around #
###################################################

(
    cd ${LIB_DIR}/
    git clone https://gitlab.com/cortext/cortext-methods/corpus-terms-indexer.git
    git clone https://gitlab.com/cortext/cortext-methods/corpus-explorer.git
)


###########################################################################
# Download data and a database from Cortext Manager, create metadata file #
###########################################################################

# WOS database on environmental sample banks:
# - https://assets.cortext.net/docs/041016bca57d0a79958a8b2b16b7eb35
# - converted to Reseaulu db:
#   - https://assets.cortext.net/docs/635c9821e532739b31648535d2dbe490

DATA_DIR="data"; mkdir $DATA_DIR

curl https://assets.cortext.net/docs/041016bca57d0a79958a8b2b16b7eb35 \
     > ${DATA_DIR}/test.zip

curl https://assets.cortext.net/docs/635c9821e532739b31648535d2dbe490 \
     > ${DATA_DIR}/test.db

echo "
alltables: [ISIFX, Funding, ISIVolume, Author, ISIABSTRACT, ISIC1State, Title, ISIPage, ISICRYear, ISIUT, Journal, Countries, ISIDOI, Address, ISILA, Cities, ISIC1Zip, Subject Category, ISIC1_1, ISIC1_0, original_filename, ISICR, ISITITLE, Cited Journal, Acknowledgement, ISIAF, Cited References, WOS Category, Abstract, Research Institutions, Journal (long), ISIID, ISITC, Cited Author, ISIRP, Year, Keywords, Publication Type, ISImonth]
corpus_type: isi
custom_period: [Standard Periods]
extension: db
file: /srv/local/documents/d47d/d47dbaed9f1820af6c756ac1364b2f19/405935/data-base-exem.db
indexed: false
origin: dataset
structure: reseaulu
tablenames: [Address, Author, Cited Author, Cited Journal, Cited References, Cities, Countries, Funding, ISIABSTRACT, ISIAF, ISIC1State, ISIC1Zip, ISIC1_0, ISIC1_1, ISICR, ISICRYear, ISIDOI, ISIFX, ISIID, ISILA, ISIPage, ISIRP, ISITC, ISITITLE, ISIUT, ISIVolume, ISImonth, Journal, Journal (long), Keywords, Publication Type, Research Institutions, Subject Category, WOS Category, Year, original_filename]
textual_fields: [Abstract, Acknowledgement, Address, ISIID, Keywords, Title]
totaltables: [ISIFX, ISIABSTRACT, ISIJOURNAL, ISIFU, ISIkeyword, ISIVolume, ISIPage, ISIUT, ISISC, ISIWC, ISIAUTHOR, ISIDOI, ISIDT, ISILA, ISIC1_1, ISIC1_0, original_filename, ISICR, ISIpubdate, ISITITLE, ISIAF, ISIID, ISITC, ISIRP, ISISO, ISImonth, ISICRAuthor, ISICRYear, ISICRJourn, ISICitedRef, ISIC1City, ISIC1Country, ISIC1Inst, ISIC1State, ISIC1Zip, Address]
uri: data-base-exem
version: 1
" > ${DATA_DIR}/test.yaml


##################################################################
# Setup some method, in this example we'll take terms extraction #
##################################################################

METHOD_NAME="terms-extraction"
SCRIPT_NAME="terms_extractor.py"

(
    cd ${LIB_DIR}
    git clone https://gitlab.com/cortext/cortext-methods/${METHOD_NAME}.git
)


#############################
# Setup the container image #
#############################

DOCKER_TAG="cortext-methods/${METHOD_NAME}-test"
(
    cd ${LIB_DIR}/${METHOD_NAME}/docker/
    docker build -t $DOCKER_TAG .
)


####################################################
# Setup a job description (paths must be absolute) #
####################################################

JOB_NAME="${METHOD_NAME}-0"
JOBS_DIR="jobs"; mkdir $JOBS_DIR
RESULTS_DIR="results/${JOB_NAME}"; mkdir -p $RESULTS_DIR

SYSTEM_PARAMS="
# sys_params:
    container: docker:${DOCKER_TAG}
    cortextlib_needs_network: no
    script_path: $(realpath ${LIB_DIR}/${METHOD_NAME}/${SCRIPT_NAME})
    corpus_file: $(realpath ${DATA_DIR}/test.db)
    result_path: $(realpath ${RESULTS_DIR})
"

if
    [[ "$METHOD_NAME" == "parser-science" ]] ;
then
     JOB_PARAMS=${SYSTEM_PARAMS}"
# usr_params:
    corpusorigin: dataset
    corpustype: isi
    jsonuniqueid: ''
    corpustype2: other
    formatting: 'default csv'
    uniquefield: yes
    paragraph: no
    paragraphid: yes
    sentenced: no
    simplify_name: yes
    yearfield: ''
    xlsyearfield: ''
    separatorxls: '***'
    secondary_separatorxls: '|&|'
    separator: '***'
    robustformatting: tabulation
    secondary_separator: '|&|'
    robustseparator: '***'
    robustsecondary_separator: '|&|'
    jsonformat: other
    yearfieldjson: ''
    yearfieldjsonml: ''
    factivatimeresolution: month
    timegranularity: year
    timegranularityjsonml: year
    timegranularitytips: year
    startingyear: '2000'
    factivastartingyear: '1990'
    startingyearjsonml: '2000'
    startingyearcsvradaraly: '2000'
    timegranularitycsv: year
    robustyearfield: ''
    xlsspecialtime: no
    xlsspecialtimeresolution: year
    europressespecialtimeresolution: year
    startingyearxls: '2000'
    startingyeareuropresse: '2000'
    csvspecialtime: no
    csvspecialtimeresolution: year
    startingyearcsv: '2000'
    robustcsvspecialtime: no
    robustcsvspecialtimeresolution: year
    startingyearrobustcsv: '2000'
    weights_tablename_json: ''
    weights_tablename: ''
    permissive_time_check: yes
" ;
elif
    [[ "$METHOD_NAME" == "terms-extraction" ]] ;
then
    JOB_PARAMS=${SYSTEM_PARAMS}"
# usr_params:
    fields_2_index:
        - Abstract
        - Title
    C_value_thres: '3.'
    nb_top: '100'
    language: en
    no_monogram: yes
    max_length: '3'
    advanced_settings_main: no
    count_method: 'sentence level'
    specificity_mode: chi2
    method_linguistic: yes
    grammaticalcriterion: 'noun phrase'
    actionable: ''
    arobase: ''
    sampling: yes
    sample_size: '10000'
    auto_index: yes
    indexed_table_name: ''
    periods: 'Standard Periods'
    nb_period: '1'
    time_cut_type: homogeneous
" ;
fi

echo "$JOB_PARAMS" > ${JOBS_DIR}/${JOB_NAME}.yaml

###############
# Now run it! #
###############

${CM_JOB_RUN} ${JOBS_DIR}/${JOB_NAME}.yaml
