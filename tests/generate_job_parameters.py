#!/usr/bin/env python3

import sys
import logging
import yaml

logger = logging.getLogger(__name__)


def get_system_params():
    """
    Generates parameters provided by cortext manager and their values by iterating
    over predefined keys, yielding each key along with a placeholder description.
    For nested parameters, the parent key and a dictionary of subkeys with their
    descriptions are yielded.

    yields:
        tuple: a tuple of the parameter key and its description, or a dictionary
        for nested parameters
    """
    manager_param_keys = [
        "job_id",
        "label",
        "project_id",
        "user_id",
        "script_id",
        "corpus_id",
        "created_at",
        "corpus_name",
        (
            "context",
            [
                "project_id",
                "analysis_id",
                "callback_json",
                "username",
                "assets_url",
            ],
        ),
    ]

    def manager_provided_description(param_key):
        return f"{param_key.replace('_', ' ')} (provided by cortext manager)"

    for element in manager_param_keys:
        if isinstance(element, (list, tuple)) and len(element) == 2:
            key, value = element
            if isinstance(value, (list, tuple)):
                yield key, {
                    subkey: manager_provided_description(subkey) for subkey in value
                }
        elif isinstance(element, str):
            yield element, manager_provided_description(element)


def get_user_params(data):
    """
    Traverses a nested dictionary structure to yield key-value pairs from
    'params' dictionaries.

    args:
      data (dict): the nested dictionary to traverse.

    yields:
      tuple: a tuple containing (key, value) for each item in the 'params'
      dictionary found during traversal.

    note:
      If the value associated with the 'params' key is not a dictionary,
      it is ignored. Lists and other non-dictionary types are also ignored.
    """
    if not isinstance(data, dict):
        return
    stack = [data]
    while stack:
        current = stack.pop()
        # add items in reverse order to process following the original ordering
        for key, value in reversed(current.items()):
            if key == "params":
                # ignore non-dictionary values for the 'param' key
                if isinstance(value, dict):
                    for param_key, param_value in value.items():
                        yield (param_key, param_value)
            # we only traverse dictionaries
            if isinstance(value, dict):
                stack.append(value)


def get_param_value(param_description):
    if "default" in param_description:
        param_value = param_description.get("default")
    elif "widget" in param_description:
        widget = param_description.get("widget")
        source = widget.get("source")
        default = widget.get("default", "")
        if source == "descriptor":
            param_value = f'{{corpus-sourced parameter. default value: "{default}"}}'
        elif source == "corpus":
            extensions = widget.get("extension")
            param_value = f"{{path to a {"/".join(extensions) + " "}file}}"
        else:
            param_value = default
        if widget.get("multiple") and not isinstance(param_value, list):
            param_value = [param_value]
    else:
        param_value = ""
    return param_value


def represent_bool(representer, data):
    """
    Representer function for PyYAML to output booleans as 'yes' or 'no'.

    args:
      representer (yaml.Representer): PyYAML representer instance.
      data (bool): boolean value to represent.

    returns:
      yaml.ScalarNode: The YAML scalar node for the boolean.

    note: PyYAML documentation at https://pyyaml.org/wiki/PyYAMLDocumentation is poor and outdated. see https://github.com/yaml/pyyaml/blob/main/lib/yaml/representer.py for interface details.
    """
    return representer.represent_scalar(
        "tag:yaml.org,2002:bool", "yes" if data else "no"
    )


params_to_fill = [
    ("script_path", "{path of the script to run}"),
    ("corpus_file", "{path of the corpus (.db) file}"),
    ("result_path", "{path of the results folder}"),
]


def from_method_conf(method_conf):
    """
    Constructs a cortext-compatible job parameters structure, represented as a
    nested dictionary, from the method configuration. User parameters and
    default/placeholder values are extracted from the method configuration, and
    cortext-provided and job-related parameters are included.

    args:
        method_conf (dict): nested dictionary representing a method configuration

    returns:
        dict: a dictionary with the parameter keys and default values or
        placeholders to be filled
    """
    result = {}

    for key, description in get_user_params(method_conf):
        value = get_param_value(description)
        if isinstance(value, str) and (value == "" or "}" in value):  # TODO manage corpus-sourced params better
            logger.debug("parameter %s must be manually given a value", key)
        result[key] = value

    for key, description in get_system_params():
        result[key] = description

    for key, description in params_to_fill:
        logger.debug("parameter %s must be manually given a value", key)
        result[key] = description

    return result


def from_method_conf_file(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        method_conf = yaml.safe_load(file)
        params_dict = from_method_conf(method_conf)
        return params_dict


def main(method_conf_path, job_params_path):
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(message)s")

    params_dict = from_method_conf_file(method_conf_path)

    safe_dumper = yaml.SafeDumper
    safe_dumper.add_representer(bool, represent_bool)

    with open(job_params_path, "w", encoding="utf-8") as method_conf_path:
        yaml.dump(
            params_dict, method_conf_path, indent=2, sort_keys=False, Dumper=safe_dumper
        )

    logger.debug("job parameters written to %s", job_params_path)
    logger.debug("open and edit %s to give values to the parameters above", job_params_path)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python generate_job_parameters.py <path_to_method_conf_yaml_path> <output_job_params_yaml_path>"
        )
        sys.exit(1)

    method_conf_path = sys.argv[1]
    job_params_path = sys.argv[2]
    main(method_conf_path, job_params_path)
